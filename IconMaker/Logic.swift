//
//  Logic.swift
//  XBFlower
//
//  Created by sixiaobo on 2018/4/1.
//  Copyright © 2018年 sixiaobo. All rights reserved.
//

import UIKit

//数学坐标,传入视图坐标
func logicPoint(point:CGPoint, center:CGPoint) -> CGPoint {
    return CGPoint(x:point.x - center.x, y:point.y - center.y)
}
//视图坐标,传入数学坐标
func viewPoint(point:CGPoint, center:CGPoint) -> CGPoint {
    return CGPoint(x:point.x + center.x, y:point.y + center.y)
}
//共同预处理,传入起始点，圆心;返回第一步偏移角(相对于标准坐标),半径平方
private func commonParam(loc:CGPoint, center:CGPoint) -> (Double, CGFloat) {
    let loc = logicPoint(point: loc, center: center)
    let R_2 = pow(loc.x, 2) + pow(loc.y, 2) //半径平方
    var firstStep = Double(atan(loc.y/loc.x))
    if loc.x == 0 {
        if loc.y < 0 {
            firstStep = .pi * 2 - .pi/2
        } else {
            firstStep = Double.pi/2
        }
    }
    if loc.x < 0 { //二三象限
        firstStep = firstStep + .pi
    }
    if loc.x > 0 && loc.y < 0 { //第四象限
        firstStep = firstStep + 2 * .pi
    }
    return (firstStep, R_2)
}
//共同处理，计算出下一个偏移点
private func nextPoint(param:(Double, CGFloat), step:Double, index:Int, center:CGPoint) -> CGPoint {
    var next = param.0 + step * Double(index)  //下一个点的偏移角
    var x = fabs(sqrt(Double(param.1) / (1.0 + pow(tan(next), 2))))
    var y = fabs(x*tan(next))
    func handle(next:Double) {
        if next > .pi/2 && next <= .pi {  //第二象限
            x = -x
        } else if next > .pi && next <= 3 * .pi/2 { //第三象限
            x = -x
            y = -y
        } else if next > 3 * .pi/2 && next < 2 * .pi {  //第四象限
            y = -y
        }
    }
    handle(next: next);
    if next >= 2 * .pi {  //超过2pi
        handle(next: next - 2 * .pi);
    }
    return viewPoint(point: CGPoint(x:x, y:y), center: center)
}

//[0]所有同轨道上的点
func arc_points(loc:CGPoint, center:CGPoint, num:Int) -> [CGPoint] {
    var arr:[CGPoint] = [loc]
    if num < 2 {
        return arr;
    }
    let step = 2 * .pi / Double(num)  //间隔角
    let param:(Double, CGFloat) = commonParam(loc: loc, center: center)
    for index in 1...num-1 {
        autoreleasepool {
            arr.append(nextPoint(param: param, step: step, index: index, center: center))
        }
    }
    return arr
}



//[1]点弧度偏移
func moveStepPoint(point:CGPoint, step:Double, center:CGPoint) -> CGPoint {
    return nextPoint(param: commonParam(loc: point, center: center), step: step, index: 1, center: center)
}


//算出一个笔划相关的所有点，矩形, pro为宽高比
func cube_locs(loc:CGPoint, cent:CGPoint, pro:CGFloat, count:Int) -> [CGPoint] {
    let wx = CGFloat(fabs(Double(loc.x - cent.x)))
    let hy = CGFloat(fabs(Double(loc.y - cent.y)))
    
    var width:CGFloat = 0
    var height:CGFloat = 0
    if wx/hy < pro {
        height = 2*hy
        width = height * pro
    }
    if wx/hy > pro {
        width = 2*wx
        height = width/pro
    }
    if wx/hy == pro {
        width = wx
        height = hy
    }
    let originX = cent.x - width/2
    let originY = cent.y - height/2
    let space = (width * 2 + height * 2) / CGFloat(count)
    
    
    var arr:[CGPoint] = []
    //递归运算
    func caculate(loc:CGPoint) {
        if arr.count >= count {
            return
        }
        arr.append(loc)
        if arr.count >= count {
            return
        }
        if loc.x == originX + width {  //在右边线
            var mark = loc.y - space
            while mark >= originY {
                autoreleasepool {
                    arr.append(CGPoint.init(x: loc.x, y: mark))
                }
                if arr.count >= count {
                    return
                }
                mark -= space
            }
            caculate(loc: CGPoint.init(x: originX + width + mark - originY, y: originY)) //去上边线
        }
        if loc.y == originY {  //在上边线
            var mark = loc.x - space
            while mark >= originX {
                autoreleasepool {
                    arr.append(CGPoint.init(x: mark, y: loc.y))
                }
                if arr.count >= count {
                    return
                }
                
                mark -= space
            }
            caculate(loc: CGPoint.init(x: originX, y: originX - mark + originY))  //去左边线
        }
        if loc.x == originX {  //在左边线
            var mark = loc.y + space
            while mark <= originY + height {
                autoreleasepool {
                    arr.append(CGPoint.init(x: loc.x, y: mark))
                }
                if arr.count >= count {
                    return
                }
                
                mark += space
            }
            caculate(loc: CGPoint.init(x: mark - originY - height + originX, y: originY + height))  //去下边线
        }
        if loc.y == originY + height {  // 在下边线
            var mark = loc.x + space
            while mark <= originX + width {
                arr.append(CGPoint.init(x: mark, y: originY + height))
                if arr.count >= count {
                    return
                }
                mark += space
            }
            caculate(loc: CGPoint.init(x: originX + width, y: originY + height - (mark - (originX + width))))  //去右边线
        }
    }
    autoreleasepool {
        caculate(loc: loc)
    }
    return arr
}


/**
 *  心形路径 中心点，cet 两边圆的半径，radius 即正方形边长的1/2
 */
func heartPath(cet:CGPoint, radius:CGFloat) -> UIBezierPath {
    let path = UIBezierPath.init()
    let p0 = CGPoint.init(x: cet.x, y: cet.y - radius * pow(2, 0.5))  //顶
    let p1 = CGPoint.init(x: cet.x + radius * pow(2, 0.5), y: cet.y)  //右
    let p2 = CGPoint.init(x: cet.x, y: cet.y + radius * pow(2, 0.5))  //底
    let p3 = CGPoint.init(x: cet.x - radius * pow(2, 0.5), y: cet.y)  //左
    
    path.move(to: p3)
    path.addCurve(to: p0, controlPoint1: pointNotCet(c: p3, p: p2), controlPoint2: pointNotCet(c: p0, p: p1))
    path.addCurve(to: p1, controlPoint1: pointNotCet(c: p0, p: p3), controlPoint2: pointNotCet(c: p1, p: p2))
    
    //    path.addArc(withCenter: pointAverage(p0: p3, p1: p0), radius: radius, startAngle: 3.0/4 * .pi, endAngle: -.pi/4, clockwise: true)
    //    path.addArc(withCenter: pointAverage(p0: p0, p1: p1), radius: radius, startAngle: -.pi * 3/4, endAngle: .pi/4, clockwise: true)
    path.addLine(to: p2)
    path.addLine(to: p3)
    path.close()
    return path
}

/**
 * 依据平均点和一个点，算另一个点
 */
func pointNotCet(c:CGPoint, p:CGPoint) -> CGPoint {
    return CGPoint.init(x: 2 * c.x - p.x, y: 2 * c.y - p.y)
}




