//
//  AppDelegate.h
//  IconMaker
//
//  Created by 司小波 on 2017/8/11.
//  Copyright © 2017年 司小波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

