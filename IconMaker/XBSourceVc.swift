//
//  XBSourceVc.swift
//  PicSearch
//
//  Created by 司小波 on 2017/8/10.
//  Copyright © 2017年 com.sixiaobo. All rights reserved.
//

import UIKit


let kwidth = UIScreen.main.bounds.size.width
let kheight = UIScreen.main.bounds.size.height

private let cachePath = "/Users/sixiaobo/Documents/AppIcon"
private let inPath = "/Users/sixiaobo/Desktop/Icons/in"

// 20    29    40    60   76  83.5 苹果icon需要的点数集合

class XBSourceVc: UIViewController {
    private let label = UILabel.init(frame: CGRect.init(x: 0, y: kheight - 80, width: kwidth, height: 80))
    private let names = UIFont.familyNames
    private let iv = UIImageView.init(frame: CGRect.init(x: 30, y: 100, width: kwidth - 60, height: kwidth - 60))
    private var mark:Int = 0
    @objc var iconDrawBlock:(CGContext, CGSize)->Void = {_,_  in}
    @objc var originIconBlock:() -> UIImage = {return UIImage()}  //图片必须是正方形，否则会变形
    var sizes = [CGSize.init(width: 20, height: 20),
                 CGSize.init(width: 29, height: 29),
                 CGSize.init(width: 40, height: 40),
                 CGSize.init(width: 60, height: 60),
                 CGSize.init(width: 76, height: 76),
                 CGSize.init(width: 83.5, height: 83.5),
                 CGSize.init(width: 28, height: 28),
                 CGSize.init(width: 108, height: 108),
                 CGSize.init(width: 512, height: 512)]
    var titDes:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = "字体演示  I love you"
        label.textColor = UIColor.red
        self.view.addSubview(label)
        self.view.addSubview(iv)
        
//        iv.image = self.iconWithSize(size: CGSize.init(width: 500, height: 500))
//        iv.contentMode = UIViewContentMode.scaleAspectFill
//        iv.clipsToBounds = true
        
        func makeBtn(act:Selector, off:CGFloat, _ title:String) {
            let drawBgtn = UIButton.init(type: UIButton.ButtonType.custom)
            drawBgtn.frame = CGRect.init(x: off, y: iv.frame.origin.y + iv.frame.size.height + 30, width: 80, height: 40);
            self.view.addSubview(drawBgtn)
            drawBgtn.addTarget(self, action: act, for: UIControl.Event.touchUpInside)
            drawBgtn.setTitle(title, for: UIControl.State.normal)
            drawBgtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            drawBgtn.backgroundColor = UIColor.black
        }
        makeBtn(act: #selector(drawAct), off: kwidth/2 - 100, "绘制图")
        makeBtn(act: #selector(iconAct), off: kwidth/2 + 20, "原生图")
    }

    
    @objc func clipAct(block:@escaping ((UIImage)->UIImage)) {
        func handle(name:String) {
            let path = inPath + "/" + name
            var image = UIImage.init(contentsOfFile: path)!
            image = block(image)
            cacheImage(image: image, name: name)
        }
        
        handle(name: "many1.jpeg")
        handle(name: "many2.jpeg")
    }
    
    
    @objc func drawAct() {
        let size = CGSize.init(width: 500, height: 500)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let ctx = UIGraphicsGetCurrentContext()
        iconDrawBlock(ctx!, size)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        iv.image = image
        iv.layer.cornerRadius = 40
        iv.clipsToBounds = true
        self.makeIcons(image: image!)
    }
    
    /**
     * 画特定图片
     */
    @objc func drawPic(size:CGSize, name:String, block:@escaping (CGContext,CGSize)->Void) {
        drawPic(size: size, name: name, handle: nil, block: block)
    }
    
    /**
     * 画特定图片
     */
    @objc func drawPic(size:CGSize, name:String, handle:((UIImage)->UIImage)?, block:@escaping (CGContext,CGSize)->Void) {
        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        let ctx = UIGraphicsGetCurrentContext()
        block(ctx!, size)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let _ = handle {
             image = handle?(image!)
        }
        self.cacheImage(image: image!, name: name)
    }
    
    
    @objc func iconAct() {
        self.iv.image = self.originIconBlock()
        self.makeIcons(image: self.iv.image!)
    }
    

    private func iconWithSize(size:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let ctx = UIGraphicsGetCurrentContext()
        let w:CGFloat = 80
        ctx?.setLineWidth(w)
        
        ctx?.addRect(CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        UIColor.white.setFill()
        ctx?.fillPath()
        UIColor.init(red: 251/255, green: 109/255, blue: 58/255, alpha: 1).setStroke()
        UIColor.init(red: 55/255, green: 162/255, blue: 250/255, alpha: 1).setFill()
        ctx?.addEllipse(in: CGRect.init(x: w/2 + 80, y: w/2 + 80, width: size.width - w - 160, height: size.height - w - 160))
        ctx?.strokePath()
        ctx?.addEllipse(in: CGRect.init(x: size.width/2 - 60, y: size.height/2 - 60, width: 120, height: 120))
        ctx?.fillPath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.makeIcons(image: image!)
        return image!
    }

    
    /**
     * 传入一张大图，生成苹果需要的logo
     */
    @objc func makeIcons(image:UIImage) {
        let sizes:[CGSize] = self.sizes
        DispatchQueue.global().async {
            for size in sizes {
                let icon = self.sizeImage(origin: image, size: size, sc: 1)
                self.cacheImage(image: icon, size: size, sc: 1)
                
                let icon2x = self.sizeImage(origin: image, size: size, sc: 2)
                self.cacheImage(image: icon2x, size: size, sc: 2)
                
                let icon3x = self.sizeImage(origin: image, size: size, sc: 3)
                self.cacheImage(image: icon3x, size: size, sc: 3)
            }
        }
    }
    
    
    /**
     * 压缩图片
     */
    func sizeImage(origin:UIImage, size:CGSize, sc:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, sc)
        origin.draw(in: CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    /**
     * 存储图片
     */
    func cacheImage(image:UIImage, size:CGSize, sc:CGFloat) {
        cacheImage(image: image, name: self.nameMake(size: size, sc: sc))
    }

    func cacheImage(image:UIImage, name:String) {
        let fm = FileManager.default
        var pointer:ObjCBool = false
        if !fm.fileExists(atPath: cachePath, isDirectory: &pointer) {
            do {
                try fm.createDirectory(atPath: cachePath, withIntermediateDirectories: true, attributes: nil)
            } catch let err {
                print(err)
            }
        }
        let data = image.pngData()
        do {
            try data?.write(to: URL.init(fileURLWithPath: cachePath + "/\(name)"))
        } catch {
            
        }
    }
    

    func nameMake(size:CGSize, sc:CGFloat) -> String {
        var name = "\(Int(size.width))x\(Int(size.height))@\(Int(sc))x.png"
        if sc == 1 {
            name = "\(Int(size.width))x\(Int(size.height)).png"
        }
        return name
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.mark+=1
        if self.mark > self.names.count - 1 {
            self.mark = 0
        }
        let font = UIFont.init(name: self.names[self.mark], size: 20)
        self.label.font = font
        print(font ?? UIFont.systemFont(ofSize: 20))
        makeMany()
        self.view.addSubview(titDes)
    }
    
    
    
    
    func makeMany() {
        for view in self.view.subviews {
            view.removeFromSuperview()
        }
        
        let scroll = UIScrollView.init(frame: CGRect.init(x: 0, y: 130, width: kwidth, height: kheight - 130))
        view.addSubview(scroll)
        scroll.backgroundColor = kcolor(55, 55, 55)
        self.view.backgroundColor = scroll.backgroundColor
        
        var max:CGFloat = 0
        func addItems(top:CGFloat, key:String) -> CGFloat {
            var micons:[UIImage] = []
            let path = "/Users/sixiaobo/Desktop/appstoreIcons/" + key
            var urls:[String]!
            do {
                urls = try FileManager.default.contentsOfDirectory(atPath: path)
            } catch let err {
                print(err)
                return top
            }
            for url in urls {
                if let image = UIImage.init(contentsOfFile: path + "/" + url) {//UIImage(named: "p\(i % 3).jpeg") {
                    micons.append(image)
                }
            }
            
            var icons:[UIImage] = []
            while micons.count > 0 {
                let idx = Int(arc4random())%micons.count
                icons.append(micons[idx])
                micons.remove(at: idx)
            }
            icons += icons
            
            let obj = LayOut()
            obj.top = top
            obj.icons = icons
            for itm in obj.frames.enumerated() {
                let img = UIImageView.init(frame: itm.element)
                img.image = icons[itm.offset]
                img.clipsToBounds = true
                img.layer.cornerRadius = LayOut.sp
                scroll.addSubview(img)
                if img.frame.maxX > max {
                    max = img.frame.maxX
                }
            }
            return obj.botom
        }
        
        
//        var off = addItems(top: 0, key: "0")
//        off = addItems(top: off + LayOut.sp, key: "1")
//        off = addItems(top: off + LayOut.sp, key: "2")
//        scroll.contentSize = CGSize.init(width: max, height: scroll.frame.height)
        
        func addTop(tit:String) {
            if titDes == nil {
                titDes = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: kwidth, height: 180))
                titDes.numberOfLines = 15
                let pra = NSMutableParagraphStyle.init()
                pra.alignment = .center
                pra.lineSpacing = 15
                let str = tit
                //let str =
                //let str = "作品展示\n"
                let ats = NSMutableAttributedString.init(string: str, attributes:
                    [NSAttributedString.Key.paragraphStyle : pra,
                     NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .thin),
                     NSAttributedString.Key.foregroundColor : UIColor.white])
                var range = (str as NSString).range(of: "\n")
                range = NSRange.init(location: 0, length: range.location)
                ats.addAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)], range: range)
                titDes.attributedText = ats
            }
        }
        
        
        func add() {
            addTop(tit: "画板与矢量图\n色彩，纹理，对称与几何构造，绘画的多维度想象")
            
            let layout = VerticalLayout()
            layout.top = 50
            DispatchQueue.global().async {
                let names = ["10.jpeg", "11.jpeg", "12.jpeg", "13.jpeg"]
                var icons:[UIImage] = []
                for name in names {
                    if let image = UIImage.init(contentsOfFile: inPath + "/" + name) {
                        icons.append(image)
                    }
                }
                layout.micons = icons
                DispatchQueue.main.async {
                    for itm in layout.frames.enumerated() {
                        let img = UIImageView.init(frame: itm.element)
                        img.image = icons[itm.offset]
                        img.layer.cornerRadius = 10
                        img.clipsToBounds = true
                        scroll.addSubview(img)
                        max = img.frame.maxX + 100
                    }
                    scroll.contentSize = CGSize.init(width: max, height: scroll.frame.height)
                }
            }
        }

        func add1() {
            addTop(tit: "析色卡与调色盘\n提取世界的颜色为我所用")
            
            let layout = ColorLayout()
            layout.top = 60
            DispatchQueue.global().async {
                let names = ["14.jpeg", "15.jpeg"]
                var icons:[UIImage] = []
                for name in names {
                    if let image = UIImage.init(contentsOfFile: inPath + "/" + name) {
                        icons.append(image)
                    }
                }
                layout.micons = icons
                DispatchQueue.main.async {
                    for itm in layout.frames.enumerated() {
                        let img = UIImageView.init(frame: itm.element)
                        img.image = icons[itm.offset]
                        scroll.addSubview(img)
                        img.clipsToBounds = true
                        img.layer.cornerRadius = 10
                        max = img.frame.maxX + 100
                    }
                    scroll.contentSize = CGSize.init(width: max, height: scroll.frame.height)
                }
            }
        }
        add()
        //add1()
    }
    
    
    
}





class LayOut {
    let h:CGFloat = 230
    var top:CGFloat = 0
    var left:CGFloat = 0
    var botom:CGFloat {
        get {
            return top + h
        }
    }
    var icons:[UIImage] = [] {
        didSet {
            frames.removeAll()
            var preRect:CGRect!
            while self.icons.count > 0 {
                var frame:CGRect!
                let icon = self.icons.removeFirst()
                let w = h * icon.size.width/icon.size.height
                if preRect == nil {
                    frame = CGRect.init(x: left, y: top, width: w, height: h)
                } else {
                    frame = CGRect.init(x: preRect.maxX + LayOut.sp, y: preRect.minY, width: w, height:h)
                }
                frames.append(frame)
                preRect = frame
            }
        }
    }
    var frames:[CGRect] = []
    
    class var sp:CGFloat {
        get {
            return 10
        }
    }
}


class VerticalLayout: LayOut {
    var micons: [UIImage] = [] {
        didSet {
            guard self.micons.count == 4 else {
                return
            }
            func w(_ i:Int) -> CGFloat {
                return self.micons[i].size.width
            }
            func h(_ i:Int) -> CGFloat {
                return self.micons[i].size.height
            }
            frames.removeAll()
            let width:CGFloat = 180
            frames.append(CGRect.init(x: kwidth/2 - width - 10, y: top, width: width, height: width * h(0)/w(0)))
            var mh = 150 * h(1)/w(1)
            frames.append(CGRect.init(x: kwidth/2 + 10, y: frames[0].midY - mh/2, width: width, height: mh))
            frames.append(CGRect.init(x: kwidth/2 - width - 10, y: frames[0].maxY + 10, width: width, height: width * h(2)/w(2)))
            mh = width * h(3)/w(3)
            frames.append(CGRect.init(x: kwidth/2 + 10, y: frames[2].midY - mh/2, width: width, height: mh))
        }
    }
    
}

class ColorLayout: LayOut {
    var micons: [UIImage] = [] {
        didSet {
            guard self.micons.count == 2 else {
                return
            }
            func w(_ i:Int) -> CGFloat {
                return self.micons[i].size.width
            }
            func h(_ i:Int) -> CGFloat {
                return self.micons[i].size.height
            }
            frames.removeAll()
            let width:CGFloat = 260
            top -= 40
            frames.append(CGRect.init(x: kwidth/2 - width/2, y: top, width: width, height: width * h(0)/w(0)))
            frames.append(CGRect.init(x: kwidth/2 - width/2, y: frames[0].maxY + 10, width: width, height: width * h(1)/w(1)))
        }
    }
    
}
