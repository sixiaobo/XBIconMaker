//
//  main.m
//  IconMaker
//
//  Created by 司小波 on 2017/8/11.
//  Copyright © 2017年 司小波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
