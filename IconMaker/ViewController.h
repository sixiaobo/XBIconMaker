//
//  ViewController.h
//  IconMaker
//
//  Created by 司小波 on 2017/8/11.
//  Copyright © 2017年 司小波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
+ (void)drawLineer:(CGContextRef)ctx pathRect:(CGRect)pathRect;

@end

