//
//  Icon.swift
//  IconMaker
//
//  Created by sixiaobo on 2019/9/7.
//  Copyright © 2019 司小波. All rights reserved.
//

import UIKit


let isEn:Bool = false


extension UIColor {
    class func angleColor(angle:CGFloat) -> UIColor {
        return UIColor.init(hue: angle/360, saturation: 0.2, brightness: 1.0, alpha: 1.0)
    }
}


let colors = [
    "1" : UIColor.angleColor(angle: 60),//kcolor(249, 255, 255),
    "3" : UIColor.angleColor(angle: 100),//kcolor(254, 255, 127),
    "10" : UIColor.angleColor(angle: 140),//kcolor(225, 255, 254),
    "5" : UIColor.angleColor(angle: 180),//kcolor(254, 255, 137),
    "0" : UIColor.angleColor(angle: 220),//kcolor(255, 209, 159),
    "6" : UIColor.angleColor(angle: 260),//kcolor(132, 255, 255),
    "2" : UIColor.angleColor(angle: 300),//kcolor(219, 255, 255),
    "7" : UIColor.angleColor(angle: 340)//kcolor(255, 217, 255)
]


class DrawPic: NSObject {
    static let app:App = .xColor
    
    class var path:String {
        get {
            switch app {
            case .artTool:
                return "/Users/sixiaobo/Downloads/artTool/use/"
            case .xColor:
                if isEn {
                    return "/Users/sixiaobo/Downloads/ex/"
                }
                return "/Users/sixiaobo/Desktop/xx/"
            case .xPhoto:
                return "/Users/sixiaobo/Downloads/XPhotoIcons/use/"
            case .xVision:
                return "/Users/sixiaobo/Downloads/vision/use/"
            }
        }
    }
    
    class var iPath:String {
        get {
            switch app {
            case .artTool:
                return "/Users/sixiaobo/Downloads/artTool/use8/"
            case .xColor:
                if isEn {
                    return "/Users/sixiaobo/Desktop/8p/"
                }
                return "/Users/sixiaobo/Desktop/8pch/"
            case .xPhoto:
                return "/Users/sixiaobo/Downloads/XPhotoIcons/use8/"
            case .xVision:
                return "/Users/sixiaobo/Downloads/vision/use8/"
            }
        }
    }
    
    class var cachePath: String {
        get {
            switch app {
            case .artTool:
                return "/Users/sixiaobo/Downloads/artTool/out/"
            case .xColor:
                return "/Users/sixiaobo/Desktop/outIcons"
            case .xPhoto:
                return "/Users/sixiaobo/Downloads/XPhotoIcons/out/"
            default:
                return ""
            }
        }
    }
    
    typealias Item = (String, String, String, Int)
    @objc class func startCreate() {
        var objs:[Item]!
        objs = [
            ("识色相机", "识别颜色名, 颜色占比，精准取色", "1", 1),
            ("色卡收集", "色卡收集，一键切换配色表", "2", 5),
            ("单色卡", "单色卡，转换格式，邻近色等", "3", 5),
            ("搜索", "颜色 + 关键词搜索上万经典配色", "4", 7),
////            ("色度区间", "区间调色，调出更舒服的配色", "6", 2),
////            ("星座配色", "算法生成星座配色", "2", 2),
//            //("算法配色", "生成星座配色，互补色等", "7", 2),
//            ("色相环", "可调配色关系的色相环", "3", 2),
//            //("配色卡分享", "支持5种样式", "50", 2),
//            ("色卡分享", "分享7种样式的色卡", "51", 2),
//            ("输入色值 + 输入标签", "输入色值，创建精确配色", "63", 6),
//            ("输入色值 + 输入标签", "输入标签，扩充配色搜索库", "52", 7),
////            ("色簿", "将喜欢的配色收集起来，分享5种样式的卡片。", "11", 2),
////            ("色簿", "多种样式的配色卡", "50", 2),
////            ("色簿", "多种样式的图片色卡", "51", 2),
//            //("配色方案", "提供邻近色，互补色等十款色相环方案。", "4", 2),
        ]
        if isEn {
            objs = [
                ("Color Camera", "Identify color name", "1", 1),
               // ("【配色薄】  ", "收集配色", "2", 0),
                ("Cards", "Collect Cards", "0", 5),
                ("Format Card", "HSB, RGB, HEX, CMYK, LAB", "5", 5),
                ("Search", "Color + Key Words = Massive Classic Colors", "10", 7),
                ("Chromatic Range", "S-Range + B-Range = More Comfortable", "6", 2),
//                /("Production Color Matching", "Production of exclusive color combinations", "20", 2),
                ("Relation Matching", "Algorithm + Adjacent Color + More", "7", 2),
                ("Constellation Color", "Algorithm + Constellation = Lucky Color", "2", 7),
                ("Hue Ring", "Hue Relation + Flexible Rotation + Math", "3", 2),
                ("Color Card", "Multiple styles", "50", 2),
                ("Picture color card", "Multiple styles", "51", 2),
//                ("Color Book", "Collect favorite color combinations and share 5 styles of cards.", "11", 2),
//                ("色簿", "Color Card", "50", 2),
//                ("色簿", "Picture Color Card", "51", 2),
            ]
        }
        
        /*
         
         ("Color Camera", "Shoot and get the color name", "1", 1),
                        // ("【配色薄】  ", "收集配色", "2", 0),
                         ("Color Card", "Collect picture color cards", "0", 5),
                         ("Color Value Card", "Convert format: CMYK，LAB，RGB，HSB，HEX", "5", 5),
                         ("Search Color Combinations", "Search classic colors with palette or key words", "10", 7),
                         ("Chromatic Range", "Range adjuster to adjust the more comfortable color", "6", 2),
         //                /("Production Color Matching", "Production of exclusive color combinations", "20", 2),
                         ("Production Color Matching", "Algorithm production constellation color matching", "2", 2),
                         ("", "Algorithm production relational color matching", "7", 7),
                         ("Hue Ring", "The first hue circle with adjustable color matching", "3", 2),
                         ("Color Book", "Collect favorite color combinations and share 5 styles of cards.", "11", 2),
         */
        
        
        drawIcons(objs: objs)
        drawIcons8Plus(objs: objs)
//        drawComponets([
//            "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c10"
//        ])
//
        drawComponets([
            ["c9", "c33","c7", "c101", "c8", "c2"],
            ["c1", "c3",  "c88", "s0", "c21"],
            ["c11", "c6", "c4", "c0", "c12"],
        ], msize: SizeX)

        drawComponets([
           ["c9", "c33","c7", "c101", "c8", "c2"],
            ["c1", "c3",  "c88", "s0", "c21"],
            ["c11", "c6", "c4", "c0", "c12"],
        ], msize: Size8)
        
        drawShow(size: SizeX)
        //drawShow(size: Size8, y: -60)
    }
    
    
     
    class func drawShow(size:CGSize, y:CGFloat = 0) {
        var size = size
        size.width /= UIScreen.main.scale
        size.height /= UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let bes = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        //kcolor(56, 56, 56).setFill()
        let bgcolor = UIColor.init(white: 0.3, alpha: 1)//kcolor(245, 245, 200)
        bgcolor.setFill()
        bes.fill()
        
        let items = [
            ("a0.jpg", "识别颜色名"),
            ("r2.png", "色相环调色"),
            ("", "配色关系调节"),
            ("u0.jpg", "色度区间调色"),
            ("u1.jpg", "算法生产配色")
        ]
        
        
        let t:CGFloat = 140 + y
        
        let pra1 = NSMutableParagraphStyle()
        pra1.alignment = .center
        let ats1 = NSAttributedString.init(string: "XColor包含的创新", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 30, weight: .thin),
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.paragraphStyle : pra1
        ])
        var r1 = CGRect.init(x: 20, y: t, width: size.width - 40, height: 31)
        ats1.draw(in: r1)
        r1.size.height = 80
        r1.origin.y -= 40/2
        let bes1 = UIBezierPath.init(roundedRect: r1, cornerRadius: 10)
        bes1.lineWidth = 3
        UIColor.white.setStroke()
        bes1.stroke()
        
        for obj in items.enumerated() {
            let top:CGFloat = t + CGFloat(obj.offset) * (100) + 150
            var off:CGFloat = size.width * 0.9/3
            let w:CGFloat = 300
            let pra = NSMutableParagraphStyle()
            pra.alignment = .left
//            if obj.offset % 2 == 0 {
//                off = size.width * 1/4
//            } else {
//                off = size.width * 3/4
//            }
            
            let color = UIColor.init(hue: CGFloat(obj.offset) * (1.0/8), saturation: 0.5, brightness: 1, alpha: 1)
            let ats = NSAttributedString.init(string: obj.element.1, attributes: [
                NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 30),
                NSAttributedString.Key.foregroundColor : UIColor.white,
                NSAttributedString.Key.paragraphStyle : pra
            ])
//            let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/mcards/" + obj.element.0)!
//            let h = w * icon.size.height/icon.size.width
//            let irec = CGRect.init(x: off - w/2, y: top - w/2, width: w, height: h)
//            if obj.offset != 0 {
//                icon.draw(in: irec)
//            } else {
//                let mw:CGFloat = 60
//                let bes = UIBezierPath.init(ovalIn: .init(x: off - mw/2, y: top - mw/2, width: mw, height: mw))
//                UIColor.red.setFill()
//                UIColor.white.setStroke()
//                bes.lineWidth = 5
//                bes.fill()
//                bes.stroke()
//            }
            
            let mw:CGFloat = 60
            let bes = UIBezierPath.init(ovalIn: .init(x: off - mw/2 - 60, y: top - mw/2 + 5, width: mw, height: mw))
            color.setFill()
            UIColor.white.setStroke()
            bes.lineWidth = 5
            bes.fill()
            bes.stroke()
            
            ats.draw(in: .init(x: off, y: top - 40/2, width: w, height: 40))
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        DrawPic.cacheImage(image: image, name: "show\(size.height).PNG")
    }
    
    
    class func drawComponets(_ names:[[String]], msize:CGSize = SizeX) {
        var size = msize
        size.width /= UIScreen.main.scale
        size.height /= UIScreen.main.scale
        size.width *= 3
         UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        let bes = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        //kcolor(56, 56, 56).setFill()
        let bgcolor = UIColor.init(white: 0.9, alpha: 1)
        bgcolor.setFill()
        bes.fill()
    
//        let sty = NSMutableParagraphStyle()
//        sty.alignment = .center
//        let ats = NSMutableAttributedString.init(string: isEn ? "Make a variety of styles of color cards" : "制作多种样式的色卡", attributes:
//        [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17),
//         NSAttributedString.Key.foregroundColor : UIColor.init(white: 0.4, alpha: 1),
//         NSAttributedString.Key.paragraphStyle : sty])
//        ats.draw(in: CGRect.init(x: 0, y: 15, width: size.width, height: 20))
        
        let h:CGFloat = (size.height - 30 * 4)/3
        let left:CGFloat = 100
        let offs:[CGPoint] = [.init(x: left, y: 30), .init(x: left, y: size.height/2 - h/2), .init(x: left, y: size.height/2 + h/2 + 30)]
        for item in names.enumerated() {
            let p = offs[item.offset]
            var xOff:CGFloat = p.x
            for obj in item.element.enumerated() {
                let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/mcards/" + obj.element + ".JPG")!
                let w = icon.size.width/icon.size.height * h
                let rect = CGRect.init(x: xOff, y: p.y, width: w, height: h)
                icon.draw(in: rect)
                corner(rect: rect, radius: 5)
                xOff = rect.maxX + 30
            }
        }
        
        let pra = NSMutableParagraphStyle()
        pra.lineSpacing = 10
        
        var str = "色\n卡\n+\n制\n作\n+\n分\n享"
        if isEn {
            str = "M\nA\nK\nE\n\nC\nA\nR\nD"
        }
        let ats = NSAttributedString.init(string: str, attributes: [
            NSAttributedString.Key.foregroundColor : UIColor.black,//UIColor.angleColor(angle: 20),
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 28, weight: .regular),
            NSAttributedString.Key.paragraphStyle : pra,
        ])
        
        let rec = ats.boundingRect(with: .init(width: 80, height: kheight), options: .usesLineFragmentOrigin, context: nil)
        ats.draw(at: .init(x: 30, y: size.height/2 - rec.size.height/2))
        
//        let line = UIBezierPath.init()
//        line.lineWidth = 1
//        line.move(to: .init(x: 75, y: 0))
//        line.addLine(to: .init(x: 75, y: size.height))
//        UIColor.white.setStroke()
//        line.stroke()
        
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        let scale = UIScreen.main.scale
        let icon1 = image.clip(rect: CGRect.init(x: 0, y: 0, width: image.size.width/3 * scale, height: image.size.height * scale))
        cacheImage(image: icon1, name: "cards\(msize.height)_1.PNG")

        let icon2 = image.clip(rect: CGRect.init(x: image.size.width/3 * scale, y: 0, width: image.size.width/3 * scale, height: image.size.height * scale))
        cacheImage(image: icon2, name: "cards\(msize.height)_2.PNG")
        
        let icon3 = image.clip(rect: CGRect.init(x: 2 * image.size.width/3 * scale, y: 0, width: image.size.width/3 * scale, height: image.size.height * scale))
        cacheImage(image: icon3, name: "cards\(msize.height)_3.PNG")
        
        UIGraphicsEndImageContext()
        
    }
    
    
    
    
    
    class func drawComponetsList(_ names:[String], msize:CGSize = SizeX) {
        var size = msize
        size.width /= UIScreen.main.scale
        size.height /= UIScreen.main.scale
        
         UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        let bes = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        //kcolor(56, 56, 56).setFill()
        let bgcolor = kcolor(245, 245, 255)
        bgcolor.setFill()
        bes.fill()
    
        let sty = NSMutableParagraphStyle()
        sty.alignment = .center
        let ats = NSMutableAttributedString.init(string: isEn ? "Make a variety of styles of color cards" : "制作多种样式的色卡", attributes:
        [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17),
         NSAttributedString.Key.foregroundColor : UIColor.init(white: 0.4, alpha: 1),
         NSAttributedString.Key.paragraphStyle : sty])
        ats.draw(in: CGRect.init(x: 0, y: 15, width: size.width, height: 20))
        
        var offs:[CGFloat] = [50, 50]
        for item in names.enumerated() {
            let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/" + item.element + ".JPG")!
            let w = (size.width - 30)/2
            
            var x:CGFloat = 10
            var y:CGFloat = offs[0]
            let h = w * icon.size.height/icon.size.width
            if offs[1] < offs[0] {
                x = size.width/2 + 5
                y = offs[1]
                offs[1] += h + 10
            } else {
                y = offs[0]
                offs[0] += h + 10
            }
            let rect = CGRect.init(x: x, y: y, width: w, height: h)
            icon.draw(in: rect)
        }
    
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        cacheImage(image: image, name: "cards\(msize.height).PNG")
    }
    
    
    static let bgColor:UIColor = UIColor.init(hue: 0, saturation: 0, brightness: 0.8, alpha: 1)
    static let txtColor:UIColor = UIColor.init(hue: 0, saturation: 0, brightness: 0.3, alpha: 1)
    static let boderColor:UIColor = UIColor.init(hue: 0, saturation: 0, brightness: 0.9, alpha: 1)
    class func drawIcons(objs:[Item]) {
        
        for obj in objs.enumerated() {
            let itm = obj.element
            var image = UIImage.init(contentsOfFile: path + itm.2 + ".png")!
            var xSize = SizeX
            xSize.width /= UIScreen.main.scale
            xSize.height /= UIScreen.main.scale
            
            UIGraphicsBeginImageContextWithOptions(xSize, false, UIScreen.main.scale)
            
            let bes = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: xSize.width, height: xSize.height))
            //kcolor(56, 56, 56).setFill()
            let bgcolor = self.bgColor//UIColor.init(hue: hue, saturation: 0.3, brightness: 0.6, alpha: 1)//kcolor(245, 245, 200)
            bgcolor.setFill()
            bes.fill()
            
            let scale:CGFloat = 1//UIScreen.main.scale
            
            
            let w = xSize.width - 60
            var rect:CGRect!
            //var color:UIColor = txtColor//colors[itm.2]!
//            if obj.element.2 == "1" {
//                color = .white
//            }
            func drawBigImage(w:CGFloat, yoff:CGFloat = 0) -> CGRect {
                let h = w * image.size.height/image.size.width
                rect = CGRect.init(x: xSize.width/2 - w/2, y: xSize.height - h + yoff + 30, width: w, height: h)
                image.cornerImg(sp: 30).draw(in: rect)
                
                let mbes = UIBezierPath.init(roundedRect: rect.border(5), cornerRadius: 30 + 5)
                mbes.lineWidth = 10
                //UIColor.randomColor
                boderColor.setStroke()
                mbes.stroke()
                
//                let bes1 = UIBezierPath.init(roundedRect: rect.border(8), cornerRadius: 30 + 8)
//                bes1.lineWidth = 8
//                //UIColor.randomColor
//                bgcolor.setStroke()
//                bes1.stroke()

//                let bbes = UIBezierPath.init(roundedRect: rect.border(0.5), cornerRadius: 30 + 0.5/2)
//                bbes.lineWidth = 0.5
//                UIColor.init(white: 0.6, alpha: 1).setStroke()
//                bbes.stroke()
                return rect
            }
            let r = drawBigImage(w: w, yoff: 0)
            let file = "jpg"
            var name:String?
            switch itm.2 {
            case "6":
                name = "u0"
            case "7":
                name = "u1"
            case "2":
                name = "u2"
//            case "52":
//                name = "2"
            default:
                break
            }
//            if let name = name {
//                let image = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/useCards/\(name).\(file)")!
//                let w:CGFloat = 280
//                let h = w * image.size.height/image.size.width
//                var rect = CGRect.init(x: xSize.width - w - 10, y: rect.minY + 220, width: w, height: h)
//                if name == "2" {
//                    let mh = r.width * image.size.height/image.size.width
//                    rect = .init(x: r.minX, y: r.minY, width: r.width, height: mh)
//                }
//
//                image.cornerImg(sp: 10).draw(in: rect)
//
//            }
            
//            switch itm.2 {
//            case "51":
//                var x:CGFloat = -20
//                for i in 0...6 {
//                    let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/useCards/picCards/\(i).JPG")!
//                    let h:CGFloat = 80
//                    let w = icon.size.width/icon.size.height * h
//                    let rect = CGRect.init(x: x, y: xSize.height * 1/3, width: w, height: h)
//                    icon.draw(in: rect)
//                    x = rect.maxX + 10
//                }
//            case "50":
//                var x:CGFloat = -100
//                for i in 0...4 {
//                    let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/useCards/colorCards/\(i).JPG")!
//                    let h:CGFloat = 80
//                    let w = icon.size.width/icon.size.height * h
//                    let rect = CGRect.init(x: x, y: rect.minY + 70, width: w, height: h)
//                    icon.draw(in: rect)
//                    x = rect.maxX + 10
//                }
//            case "5":
//                var x:CGFloat = -20
//                for i in 0...6 {
//                    let icon = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/useCards/singleCards/\(i).JPG")!
//                    let h:CGFloat = 80
//                    let w = icon.size.width/icon.size.height * h
//                    let rect = CGRect.init(x: x, y: rect.minY + 20, width: w, height: h)
//                    icon.draw(in: rect)
//                    x = rect.maxX + 10
//                }
//            default:
//                break
//            }
            
            
            //255,255,228
            
            let str = itm.1// + "\n" + itm.1
            let sty = NSMutableParagraphStyle()
            sty.alignment = .center
            sty.lineSpacing = 15
            let ats = NSMutableAttributedString.init(string: str, attributes:
                [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26, weight: .bold),
                 NSAttributedString.Key.foregroundColor : txtColor,
                 NSAttributedString.Key.paragraphStyle : sty])
            
//            let range = (str as NSString).range(of: itm.1)
//            let pra = NSMutableParagraphStyle()
//            pra.alignment = .right
//            pra.lineSpacing = 5
            
//            var font =  UIFont.systemFont(ofSize: 22, weight: .thin)
////            if itm.2 == "52" {
////                font = UIFont.systemFont(ofSize: 26, weight: .bold)
////            }
//            ats.addAttributes([NSAttributedString.Key.font : font,
//                               NSAttributedString.Key.paragraphStyle : pra], range: range)

            var textRect = CGRect.init(x: 10, y: 0, width: xSize.width - 20, height: 0)
            let rec = ats.boundingRect(with: textRect.size, options: .usesLineFragmentOrigin, context: nil)
            textRect.size.height = rec.height
            textRect.origin.y = ((rect.origin.y) - textRect.height)/2
            ats.draw(in: textRect)
            

            
//            let mbes = UIBezierPath.init(roundedRect: .init(x: textRect.origin.x, y: textRect.origin.y - 10, width: textRect.size.width, height: textRect.height + 20), cornerRadius: 5)
//            mbes.lineWidth = 0.5
//            color.setStroke()
//            mbes.stroke()
            
//            let line = UIBezierPath.init()
//           line.lineWidth = 1
//           line.move(to: .init(x: 0 , y: t))
//           line.addLine(to: .init(x: xSize.width, y: t))
//           UIColor.white.setStroke()
//           line.stroke()
            
            image = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            cacheImage(image: image, name: itm.2 + ".PNG")
        }
    }
    
    
    
    class func corner(rect:CGRect, radius:CGFloat = 30, sp:CGFloat = 0) {
        var rect = rect
        rect.size.width += sp
        let mbes = UIBezierPath.init(rect: rect)
        mbes.lineWidth = 0
        let nbes = UIBezierPath.init(roundedRect: rect, cornerRadius: radius)
        mbes.append(nbes.reversing())
        //kcolor(245, 245, 245).setFill()
        mbes.fill()
    }
    
    class func drawIcons8Plus(objs:[Item]) {
        for itm in objs {
            var iSize = Size8
            iSize.width /= UIScreen.main.scale
            iSize.height /= UIScreen.main.scale
            var image = UIImage.init(contentsOfFile: iPath + itm.2 + ".png")!
            UIGraphicsBeginImageContextWithOptions(iSize, false, UIScreen.main.scale)
            let bes = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: iSize.width, height: iSize.height))
            let bgcolor = UIColor.init(white: 0.3, alpha: 1)
            bgcolor.setFill()
            bes.fill()
            
           // let scale:CGFloat = 1//UIScreen.main.scale
            

            let color = UIColor.white//colors[itm.2]!
            let w = iSize.width - 130
            let h = w * image.size.height/image.size.width
            var rect = CGRect.init(x: iSize.width/2 - w/2, y: iSize.height - h, width: w, height: h)
            
            func drawBigImage(w:CGFloat, yoff:CGFloat = 0) -> CGRect {
                let h = w * image.size.height/image.size.width
                rect = CGRect.init(x: iSize.width/2 - w/2, y: iSize.height - h + yoff, width: w, height: h)
                
                var mrect = rect
                mrect.origin.y -= 50
                mrect.size.height += 100
                
                let mbes = UIBezierPath.init(roundedRect: mrect.border(10), cornerRadius: 30)
                mbes.lineWidth = 0
                //UIColor.randomColor
                color.setFill()
                mbes.fill()
                
                
                
                let pbes = UIBezierPath.init(ovalIn: .init(x: rect.midX - 45, y: rect.minY - 30 + 4 - 3, width: 6, height: 6))
                bgcolor.setFill()
                pbes.fill()

                let cbes = UIBezierPath.init(ovalIn: .init(x: rect.midX - 2, y: rect.minY - 30 - 10, width: 4, height: 4))
                cbes.fill()

                
                
                bgcolor.setStroke()
                
                let lbes = UIBezierPath.init()
                lbes.move(to: .init(x: rect.midX - 25, y: rect.minY - 30 + 4))
                lbes.addLine(to: .init(x: rect.midX + 25, y: rect.minY - 30 + 4))
                lbes.lineWidth = 3
                lbes.lineCapStyle = .round
                lbes.stroke()
                
                let bes = UIBezierPath.init(ovalIn: .init(x: rect.midX - 18, y: rect.maxY + 10, width: 18 * 2, height: 18 * 2))
                bes.lineWidth = 1
                bes.stroke()
        
                image.cornerImg(sp: 8).draw(in: rect)
                let bbes = UIBezierPath.init(roundedRect: rect.border(0.5), cornerRadius: 8 + 0.5/2)
                bbes.lineWidth = 0.5
                UIColor.init(white: 0.6, alpha: 1).setStroke()
                bbes.stroke()
                
                return rect
            }
            let r = drawBigImage(w: w, yoff: -70)
            let file = "jpg"
            var name:String?
            switch itm.2 {
            case "6":
                name = "u0"
            case "7":
                name = "u1"
            case "2":
                name = "u2"
//            case "52":
//                name = "2"
            default:
                break
            }
            if let name = name, let image = UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/useCards/\(name).\(file)") {
                let w:CGFloat = 280
                let h = w * image.size.height/image.size.width
                var rect = CGRect.init(x: iSize.width - w - 10, y: rect.minY + 220, width: w, height: h)
                if name == "2" {
                    let mh = r.width * image.size.height/image.size.width
                    rect = .init(x: r.minX, y: r.minY, width: r.width, height: mh)
                }
                
                image.cornerImg(sp: 10).draw(in: rect)
            }
            
            let str = itm.1// + "\n" + itm.1
            let sty = NSMutableParagraphStyle()
            sty.alignment = .center
            sty.lineSpacing = 15
            let ats = NSMutableAttributedString.init(string: str, attributes:
                [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 25, weight: .bold),
                 NSAttributedString.Key.foregroundColor : color,//UIColor.init(white: 0.3, alpha: 1),
                 NSAttributedString.Key.paragraphStyle : sty])
            
//            let range = (str as NSString).range(of: itm.1)
//            let pra = NSMutableParagraphStyle()
//            pra.alignment = .right
//            pra.lineSpacing = 5
//            ats.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .thin),
//                               NSAttributedString.Key.paragraphStyle : pra], range: range)
           
            
            var textRect = CGRect.init(x: 20, y: 0, width: iSize.width - 40, height: 0)
            let rec = ats.boundingRect(with: textRect.size, options: .usesLineFragmentOrigin, context: nil)
            textRect.size.height = rec.height
            textRect.origin.y = (rect.origin.y)/2 - textRect.height/2 - 30
            ats.draw(in: textRect)
            
//            let t = rect.minY - 20
//            let line = UIBezierPath.init()
//            line.lineWidth = 1
//            line.move(to: .init(x: 0 , y: t))
//            line.addLine(to: .init(x: iSize.width, y: t))
//            UIColor.white.setStroke()
//            line.stroke()
            
            image = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            cacheImage(image: image, name: itm.2 + "8.png")
        }
    }
    
    
    
    func mergeSmall(xSize:CGSize, image:UIImage, rect:CGRect, itm:Item) {
        var drawRect = rect
        var names:[String] = []
        var sp:CGFloat = 30
        var mw:CGFloat = 200
        var y:CGFloat = drawRect.minY + 160
        let topOff:CGFloat = 30
        var corner:CGFloat = 10
        var file:String = "jpg"
        switch itm.2 {
        case "0":
            //names = ["b0", "b1"]
            y = drawRect.minY + 120
            sp = 60
            mw = 150
        case "1":
            //names = ["a0", "a1"]
            corner = 20
            mw = 70
        case "2":
            //names = ["u1", "u2"]
            mw = 160
        case "10":
            //names = ["e0", "e1"]
            sp = 100
        case "5":
            //names = ["s0", "s1"]
            mw = 150
            y = drawRect.minY + 120
            sp = 70
        case "3":
            //names = ["r2", "r0"]
            mw = 120
            sp = 0
            file = "png"
            y = drawRect.minY + 80
        case "6":
            //names = ["u0"]
            y += 30
            mw = 220
        case "11":
            //names = ["f0", "f1"]
            sp = 90
            y += 50
        default:
            break
        }
        
        if names.count > 0 {
            drawRect.size.width *= 0.8
            drawRect.size.height *= 0.8
            drawRect.origin.x = xSize.width/2 - drawRect.size.width/2
            drawRect.origin.y += topOff
            image.cornerImg(sp: 30).draw(in: drawRect)
            
            let icons = names.map { (name) -> UIImage in
                return UIImage.init(contentsOfFile: "/Users/sixiaobo/Downloads/xx/mcards/\(name).\(file)")!
            }
            let off = xSize.width - mw - 10
            
            var recs:[CGRect] = []
            for item in icons.enumerated() {
                var w = mw
                if names[item.offset] == "r0" {
                    w += 70
                }
                let rec = CGRect.init(x: item.offset % 2 == 0 ? off : 10, y: y, width: w, height: w * item.element.size.height/item.element.size.width)
                recs.append(rec)
                y = rec.maxY + sp
            }
            
            for itm in icons.enumerated() {
                itm.element.cornerImg(sp: corner).draw(in: recs[itm.offset])
                //corner(rect: recs[itm.offset])
            }
            
        } else {
            image.cornerImg(sp: 30).draw(in: drawRect)
        }
    }
    
    
    
    class func cacheImage(image:UIImage, name:String) {
        let fm = FileManager.default
        var pointer:ObjCBool = false
        if !fm.fileExists(atPath: cachePath, isDirectory: &pointer) {
            do {
                try fm.createDirectory(atPath: cachePath, withIntermediateDirectories: true, attributes: nil)
            } catch let err {
                print(err)
            }
        }
        let data = image.pngData()
        do {
            try data?.write(to: URL.init(fileURLWithPath: cachePath + "/\(name)"))
        } catch {
            
        }
    }
}



extension UIImage {
    func clip(rect:CGRect) -> UIImage {
        var ref = self.cgImage
        ref = ref!.cropping(to: rect)
        return UIImage.init(cgImage: ref!)
    }
    
    func cornerImg(sp:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        let rect = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: sp * UIScreen.main.scale)
        bes.addClip()
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


extension CGRect {
    func border(_ sp:CGFloat) -> CGRect {
        return .init(x: minX - sp, y: minY - sp, width: width + 2 * sp, height: height + 2 * sp)
    }
}


extension CGFloat {
    static var randomFen:CGFloat {
        get {
            return CGFloat(arc4random() % 10001 + 6000)/10000
        }
    }
}


extension UIColor {
    class var randomColor:UIColor {
        get {
            return UIColor(red: .randomFen, green: .randomFen, blue: .randomFen, alpha: 1)
        }
    }
}
