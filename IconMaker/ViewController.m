//
//  ViewController.m
//  IconMaker
//
//  Created by 司小波 on 2017/8/11.
//  Copyright © 2017年 司小波. All rights reserved.
//

#import "ViewController.h"
#import "IconMaker-Swift.h"

static BOOL isX = true;

static NSString *impPath = @"/Users/sixiaobo/Desktop/Icons/imp/";
static NSString *hollowPath = @"/Users/sixiaobo/Desktop/Icons/hollow/";
static NSString *rootPath = @"/Users/sixiaobo/Desktop/Icons/";

#define fillColor [UIColor colorWithRed:245.f/255 green:245.f/255 blue:245.f/255 alpha:1]//[UIColor colorWithRed:54.f/255 green:55.f/255 blue:55.f/255 alpha:1]
//裁剪图片
UIImage * imageWithRect(CGRect rect, CGFloat scale, UIImage *image) {
    rect.size.width *= scale;
    rect.size.height *= scale;
    rect.origin.x *= scale;
    rect.origin.y *= scale;
    
    CGImageRef imageRef = image.CGImage;
    CGImageRef imageRefRect = CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
    return sendImage;
}


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    XBSourceVc *source = [[XBSourceVc alloc] init];
    //source.sizes = @[[NSValue valueWithCGSize:CGSizeMake(300, 500)]];
    source.iconDrawBlock = ^(CGContextRef _Nonnull ctx, CGSize size) {
        //[Func sorterWithCtx:ctx size:size];
        //[Func focustackWithCtx:ctx size:size];
            //[Func ctxHandleWithCtx:ctx size:size];
            //[Func iconCtxHandleWithCtx:ctx size:size];
            //[Func process1WithCtx:ctx size:size];
        //[Func nodeNoteWithCtx:ctx size:size];
        //[Func mindNoteWithCtx:ctx size:size];
            //[Func tastackWithCtx:ctx size:size];
            //[Func tastackUseWithCtx:ctx size:size];
            //[Func tastackProWithCtx:ctx size:size];
            [Func xcolorWithCtx:ctx size:size];
        //[Func draftWithCtx:ctx size:size];
            //[Func impWithCtx:ctx size:size];
            //[Func visionWithCtx:ctx size:size];
            //[Func colorCoWithCtx:ctx size:size];
        //[Func colorDeskWithCtx:ctx size:size];
        //[Func xcolorProWithCtx:ctx size:size];
        //[Func colorMaxFlowerWithCtx:ctx size:size];
            //[Func icliperWithCtx:ctx size:size];
        //[Func histackWithCtx:ctx size:size];
        //[Func xPhotoWithCtx:ctx size:size];
    };
    
    source.originIconBlock = ^UIImage * _Nonnull{
        return [UIImage imageNamed:@"girl.jpg"];
    };
    
    [self addChildViewController:source];
    [self.view addSubview:source.view];
    
    
//    NSArray<NSArray<NSString *> *> *tits =@[@[@"XVision\n可编程的三维视觉艺术", @"0.jpeg"],
//                                            @[@"幻视编程\n亲手设计你的星辰宇宙", @"1.jpeg"],
//                                            @[@"印象派\n像素与色彩的新定义", @"2.jpeg"],
//                                            @[@"印象派\n给照片换个颜色吧", @"3.jpeg"],
//                                            @[@"镂空\n绘制一个趣味图案，与照片融合", @"4.jpeg"],
//                                            @[@"析色卡\n拍摄，识别，使用万物的颜色", @"5.jpeg"],
//                                            @[@"画板与矢量图\n色彩,纹理,对称与几何构造，绘画的多维度想象", @"6.jpeg"],
//                                            @[@"艺术工具\n新工具创造新艺术", @"7.jpeg"],
//                                            @[@"画板\n算法助您一臂之力，绘画从未如此简单", @"16.jpeg"],
//                                            @[@"矢量图\n极简步骤绘制出设计师级别的几何图案", @"17.jpeg"],
//                                            @[@"相框\n给照片绘个相框吧", @"19.jpeg"],
//                                            @[@"绘迹本，装饰，时钟\n让文字回归感性，让照片充满趣味，动手造个时钟", @"18.jpeg"]
//
//                      ];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        for (NSInteger i = 0; i < tits.count; i++) {
//            NSArray *arr = tits[i];
//            [self makePic:arr.firstObject name:arr.lastObject i:i source:source];
//        }
//    });
//

    
//    [source clipActWithBlock:^UIImage * _Nonnull(UIImage * _Nonnull image) {
//        return imageWithRect(CGRectMake(0, 350, 1242, 2208), 1, image);
//    }];
    
    //[DrawPic startCreate];
    
}
    

+ (void)drawLineer:(CGContextRef)ctx pathRect:(CGRect)pathRect {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat locations[] = { 0.0, 1.0 };
    
    CGColorRef startColor =[UIColor greenColor].CGColor;
    
    CGColorRef endColor =[UIColor redColor].CGColor;
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    //具体方向可根据需求修改
    CGPoint startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMidY(pathRect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMidY(pathRect));
        CGContextSaveGState(ctx);
    CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(ctx);
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

- (void)makePic:(NSString *)tit name:(NSString *)name i:(NSInteger)i source:(XBSourceVc *)source {
    BOOL list = false;
    if (i == 3 || i == 4 || i == 0 || i == 1 || i == 5 || i == 10 || i == 9 || i == 11) {
        list = true;
    }
    
//    if (i == 0) {
//        list = true;
//    }
    
    BOOL isTop = false;
    
    //1242 x 2688
    CGSize size;
    if (isX) {
        size = CGSizeMake(1242, 2688);
    } else {
        size = CGSizeMake(1242, 2208);
    }
    BOOL isMax = isX;
    UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"/Users/sixiaobo/Desktop/Icons/%@/%@", isX ? @"in" : @"in_8", name]];
    CGFloat scale = size.width/UIScreen.mainScreen.bounds.size.width;
    [source drawPicWithSize:size name:name block:^(CGContextRef _Nonnull ctx, CGSize size) {
        CGContextAddRect(ctx, CGRectMake(0, 0, size.width, size.height));
        [fillColor setFill];
        CGContextFillPath(ctx);
        CGFloat sp = 60;
        if (!isMax) {
            sp = -100;
        }
        CGFloat h = (size.width + sp) * (img.size.height/img.size.width);
        CGFloat top = (size.height - h)/2;
        CGRect mrect = CGRectMake(-sp/2, top, size.width + sp, h);
        if (list) {
            mrect.size.height *= 0.8;
            mrect.size.width *= 0.8;
            mrect.origin.y += mrect.size.height * 0.2;
            mrect.origin.x = -30;
            if (isMax) {
                mrect.origin.x = -50;
            }
        }
        
        if (isTop) {
            CGFloat sc = 0.8;
            if (i == 5) {
                sc = 0.7;
            }
            mrect.size.height *= sc;
            mrect.size.width *= sc;

            mrect.origin.x = size.width * 1/2 - mrect.size.width/2;
        }
        
        [img drawInRect:mrect];
        
        if (list) {
            NSString *path = impPath;
            if (i == 4) {
                path = hollowPath;
            }
            if (i == 0) {
                path = [NSString stringWithFormat:@"/Users/sixiaobo/Desktop/Icons/visionList%@/", isX ? @"" : @"_8"];
            }
            if (i == 1) {
                path = [NSString stringWithFormat:@"/Users/sixiaobo/Desktop/Icons/programList%@/", isX ? @"" : @"_8"];
            }
            if (i == 5) {
                path = @"/Users/sixiaobo/Desktop/Icons/colorList/";
            }
            if (i == 10) {
                path = @"/Users/sixiaobo/Desktop/Icons/boundsList/";
            }
            if (i == 9) {
                path = @"/Users/sixiaobo/Desktop/Icons/vectorList/";
            }
            if (i == 8) {
                path = @"/Users/sixiaobo/Desktop/Icons/boxList/";
            }
            if (i == 11) {
                path = @"/Users/sixiaobo/Desktop/Icons/handtxtList/";
            }
            [self drawIcons:size path:path];
        }
        
        NSMutableParagraphStyle *tstyle = [NSMutableParagraphStyle new];
        tstyle.alignment = NSTextAlignmentCenter;
        if (list) {
            tstyle.alignment = NSTextAlignmentLeft;
        }
        tstyle.lineSpacing = 15 * scale;
        if (isTop) {
            tstyle.lineSpacing = 20;
        }
        
        CGFloat bs = 18 * scale;
        if (isMax) {
            bs = 20 * scale;
        }
        
        NSDictionary *at = @{NSFontAttributeName : [UIFont systemFontOfSize:bs weight:UIFontWeightThin],
                             NSParagraphStyleAttributeName : tstyle,
                             NSForegroundColorAttributeName : UIColor.blackColor
                             };
        NSMutableAttributedString *tat = [[NSMutableAttributedString alloc] initWithString:tit attributes:at];
        NSString *sub = [tit componentsSeparatedByString:@"\n"].firstObject;
        NSDictionary *subats = @{NSFontAttributeName : [UIFont systemFontOfSize:bs weight:UIFontWeightBold]};
        [tat addAttributes:subats range:[tit rangeOfString:sub]];
        CGRect rect = [tat boundingRectWithSize:CGSizeMake(list ? (size.width * 2/3) : size.width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        CGFloat mtop = 220;
        if (!isMax) {
            mtop = 140;
        }
        if (list) {
            mtop = size.height * 1/5.5;
            if (!isMax) {
                mtop = size.height * 1/7;
            }
        }
        if (isTop && !isMax) {
            mtop = rect.size.height/2 + 20;
        }
        
        CGFloat y = mtop - rect.size.height/2;
        if (isMax && isTop) {
            y -= 15 * scale;
        }
        [tat drawInRect:CGRectMake(list ? 70 : 0,  y, list ? size.width * 1.8/3 : size.width, rect.size.height)]; //CGRectMake(0, 200, size.width, bs + 10)
        if (isTop) {
//            if (i == 5) {
//                NSArray *images = [self imagesFromPath:[rootPath stringByAppendingString:@"colorPic/"]];
//                UIImage *image = images.firstObject;
//                CGFloat w = mrect.size.width * 0.9;
//                CGFloat h = w * image.size.height/image.size.width;
//                CGRect botomRect = CGRectMake(size.width/2 - w/2, mrect.origin.y + mrect.size.height + 30, w, h);
//                [image drawInRect:botomRect];
//                [self drawCorner:botomRect scale:scale];
//
////                UIImage *rImage = images.lastObject;
////                CGRect right = CGRectMake(size.width/2 + 5 * scale, botomRect.origin.y, botomRect.size.width, botomRect.size.width * rImage.size.height/rImage.size.width);
////                [rImage drawInRect:right];
////                [self drawCorner:right scale:scale];
//            } else {
            
            NSString *path = nil;
            if (i == 5) {
                path = @"colorPic/";
            } else if (i == 8) {
                path = @"boxPic/";
            } else {
                path = @"vectorPic/";
            }
                NSArray *images = [self imagesFromPath:[rootPath stringByAppendingString:path]];
                UIImage *image = images.firstObject;
                CGFloat w = mrect.size.width * 0.7;
//                if (i == 5) {
//                    w = mrect.size.width * 0.5;
//                }
                CGFloat h = w * image.size.height/image.size.width;
                CGRect botomRect = CGRectMake(size.width/2 - w/2, mrect.origin.y + mrect.size.height + 30, w, h);
                [image drawInRect:botomRect];
                [self drawCorner:botomRect scale:scale];
            //}
        }
    }];
}


- (void)drawCorner:(CGRect)botomRect scale:(CGFloat)scale {
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:botomRect];
    UIBezierPath *resv = [UIBezierPath bezierPathWithRoundedRect:botomRect cornerRadius:10 * scale];
    [path appendPath:resv.bezierPathByReversingPath];
    [fillColor setFill];
    [path fill];
}


- (NSArray<UIImage *> *)imagesFromPath:(NSString *)path {
    NSError *err = nil;
    NSArray<NSString *> *names = [NSFileManager.defaultManager contentsOfDirectoryAtPath:path error:&err];
    if (err) {
        return nil;
    }
    names = [names sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSString *str1 = obj1;
        NSInteger idx1 = [str1 componentsSeparatedByString:@"."].firstObject.integerValue;
        
        NSString *str2 = obj2;
        NSInteger idx2 = [str2 componentsSeparatedByString:@"."].firstObject.integerValue;
        
        if (idx1 > idx2) {
            return NSOrderedDescending;
        }
        return NSOrderedAscending;
    }];
    NSMutableArray<UIImage *> *images = [NSMutableArray array];
    [names enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj hasPrefix:@"."] && [obj containsString:@"."]) {
            [images addObject:[UIImage imageWithContentsOfFile:[path stringByAppendingString:obj]]];
        }
    }];
    return images;
}


#pragma mark- list icons
- (void)drawIcons:(CGSize)size path:(NSString *)path {
    __block CGFloat off = 0;
    NSArray<UIImage *> *images = [self imagesFromPath:path];
    [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect rect = CGRectMake(size.width * 3.1/4, off, 200, 200 * obj.size.height/obj.size.width);
        [obj drawInRect:rect];
        off = rect.size.height + rect.origin.y + 15;
    }];
}

    
    




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


+ (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    
    CGRect pathRect = CGPathGetBoundingBox(path);
    
    //具体方向可根据需求修改
    CGPoint startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMidY(pathRect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMidY(pathRect));
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}



    
    

@end
