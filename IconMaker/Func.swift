//
//  Func.swift
//  IconMaker
//
//  Created by sixiaobo on 2018/6/21.
//  Copyright © 2018年 司小波. All rights reserved.
//

import UIKit


enum App {
    case xVision
    case xColor
    case xPhoto
    case artTool
}

extension UIColor {
    class var xbpuple:UIColor {
        return UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1)
    }
}





extension CGRect {
    func edgeRect(left:CGFloat, right:CGFloat, top:CGFloat, botom:CGFloat) -> CGRect {
         return CGRect.init(x: minX + left, y: minY + top, width: width - left - right, height: height - top - botom)
    }
    /**
     * 适配尺寸的rect
     */
    func fitRect(siz:CGSize, off:CGFloat) -> CGRect {
        let msize = CGSize.init(width: size.width - 2 * off, height: size.height - 2 * off)
        if siz.width/siz.height >= msize.width/msize.height {
            let h = msize.width * siz.height/siz.width
            return CGRect.init(x: off + minX, y: off + msize.height/2 - h/2 + minY, width: msize.width, height: h)
        } else {
            let w = msize.height * siz.width/siz.height
            return CGRect.init(x: off + msize.width/2 - w/2 + minX,
                               y: off + minY, width: w, height: msize.height)
        }
    }
}


func kcolor(_ r:CGFloat,_ g:CGFloat,_ b:CGFloat, _ alpha:CGFloat = 1) -> UIColor {
    return UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: alpha)
}

let blue:UIColor = kcolor(150, 150, 255)
let yellow:UIColor = kcolor(253, 224, 1)

let SizeX = CGSize(width: 1242, height: 2688)
let Size8 = CGSize(width: 1242, height: 2208)


class Func: NSObject {
    /**
     * ArtTool
     */
    @objc class func ctxHandle(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(white: 0.2, alpha: 1).setFill()
        bes.fill()
        let s:CGFloat = 200
        let name = "Party LET"//"Zapfino"//"Chalkduster"//"Academy Engraved LET"//"Savoye LET" //"Party LET"
        let font = UIFont.init(name: name, size: s)!
        
        let pra = NSMutableParagraphStyle()
        pra.alignment = .center
        var dic = [NSAttributedString.Key.font:font,
                   NSAttributedString.Key.paragraphStyle : pra]
        
        func txt(top:Bool, str:String) {
            let color = top ? blue : yellow
            dic[NSAttributedString.Key.foregroundColor] = color
            let ats = NSMutableAttributedString.init(string: str, attributes: dic)
            let siz = ats.size()
            var rect = CGRect.init(x: size.width * 1/10, y: size.height/2 - siz.height * 3.1/4, width: size.width/2, height: siz.height)
            if !top {
                rect = CGRect.init(x: size.width/2 - size.width * 1.5/10, y: size.height/2, width: size.width/2, height: siz.height)
            }
            ats.draw(in: rect)
        }
        
        txt(top: true, str: "Art")
        txt(top: false, str: "Tool")
    }
    
    /**
     * process
     */
    @objc class func process(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(white: 1, alpha: 1).setFill()
        bes.fill()
        let s:CGFloat = 120
        //let name = "Party LET"//"Zapfino"//"Chalkduster"//"Academy Engraved LET"//"Savoye LET" //"Party LET"
        let font = UIFont.systemFont(ofSize: s, weight: .thin)
        
        let pra = NSMutableParagraphStyle()
        pra.alignment = .center
        var dic = [NSAttributedString.Key.font:font,
                   NSAttributedString.Key.paragraphStyle : pra]
        
        dic[NSAttributedString.Key.foregroundColor] = UIColor.black
        let ats = NSMutableAttributedString.init(string: "Process", attributes: dic)
        let siz = ats.size()
        let rec = CGRect.init(x: 0, y: size.height/2 - siz.height/2, width: size.width, height: siz.height)
        ats.draw(in: rec)
    }
    
    /**
     * tastack
     */
    @objc class func tastack(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(white: 1, alpha: 1).setFill()
        bes.fill()
        let w:CGFloat = size.width * 1/2
        let h:CGFloat = size.height * 1/4
        
        let rd: CGFloat = 20
        let rec = CGRect.init(x: size.width/2 - h/2, y: size.height/2 - w/2, width: h, height: w)
        let bes1 = UIBezierPath.init(roundedRect: rec, cornerRadius: 15)
        //bes1.lineWidth = 4//h * 1/5
        //UIColor.init(white: 0.4, alpha: 1).setStroke()
        //bes1.stroke()
        UIColor.init(white: 0.3, alpha: 1).setFill()
        bes1.fill()
        
        
        let sp:CGFloat = 20
        let bes2 = UIBezierPath.init(roundedRect: CGRect.init(x: rec.origin.x - sp, y: rec.origin.y - sp, width: rec.width + sp * 2, height: rec.height + sp * 2), cornerRadius: rd)
        bes2.lineWidth = 2
        UIColor.init(white: 0.3, alpha: 1).setStroke()
        bes2.stroke()
        
        let mw:CGFloat = 50//(rec.height - sp * 4)/3
        let ww:CGFloat = mw//rec.width - sp * 2
        
        func addPoint(cx:CGFloat, color:UIColor) {
            let bes = UIBezierPath.init(roundedRect: .init(x: rec.midX - ww/2, y: cx - mw/2, width: ww, height: mw), cornerRadius: mw/2)
            color.setFill()
            bes.fill()
            //bes.stroke()
        }
        let s:CGFloat = 0.5
        let b:CGFloat = 1
        
        let y0 = rec.minY + rec.height * 1/5, y1 = rec.midY, y2 = rec.minY + rec.height * 4/5
        
        
        
        func addLine(isTop:Bool) {
            let lf:CGFloat = !isTop ? rec.minX - sp : rec.maxX + sp
            let tp:CGFloat = isTop ? y0 : y2
            let p = CGPoint.init(x: lf, y: tp)
            
            let lbes = UIBezierPath.init()
            
            lbes.move(to: p)
            lbes.addLine(to: .init(x: isTop ? size.width : 0, y: tp))
            lbes.lineWidth = 2
            lbes.stroke()
            
            let pw:CGFloat = 2
            let pBes = UIBezierPath.init(ovalIn: .init(x: p.x - pw/2, y: p.y - pw/2, width: pw, height: pw))
            UIColor.init(white: 0.3, alpha: 1).setFill()
            pBes.fill()
        }
        
        addLine(isTop: false)
        addLine(isTop: true)
        
        addPoint(cx: y0, color: .init(hue: 0/360, saturation: s, brightness: b, alpha: 1))
        addPoint(cx: y1, color: .init(hue: 60/360, saturation: s, brightness: b, alpha: 1))
        addPoint(cx: y2, color: .init(hue: 120/360, saturation: s, brightness: b, alpha: 1))
    }
    
    @objc class func histack(ctx:CGContext, size:CGSize) {
        let bgColor = UIColor.systemGreen
        bgColor.setFill()
        ctx.fill(.init(x: 0, y: 0, width: size.width, height: size.height))
        //        UIColor.init(white: 1, alpha: 0.95).setFill()
        //        ctx.fill(.init(x: 0, y: 0, width: size.width, height: size.height))
        let color = UIColor.white
        var (r,g,b):(CGFloat, CGFloat, CGFloat) = (0, 0, 0)
        color.getRed(&r, green: &g, blue: &b, alpha: nil)
        let c = UIColor(red: r, green: g, blue: b, alpha: 0.6)
        //mergeShadow(ctx: ctx, color: c, radius: 30)
        
        let off:CGFloat = size.width * 1/4
        let yoff:CGFloat = size.width * 1/3
        let rect = CGRect.init(x: off, y: yoff, width: size.width - 2*off, height: size.height - 2*yoff)
        let corner:CGFloat = rect.width * 1/40
        //        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: corner * 4)
        //        //UIColor(hue: 50/360, saturation: 0.3, brightness: 0.85, alpha: 1)
        //        bgColor.setFill()
        //        bes.fill()
        
        let sp:CGFloat = rect.width * 1/30
        
        let light = UIColor.init(white: 1, alpha: 0.8)
        
        let h = (rect.height - (rect.height - sp * 9) - sp * 4)/2
        
        let rect1 = rect.edgeRect(left: sp, right: sp * 2 + h, top: sp, botom: sp * 8)
        let bes1 = UIBezierPath.init(roundedRect: rect1, cornerRadius: corner)
        light.setFill()
        bes1.fill()
        
        if #available(iOS 13.0, *) {
            let icon = UIImage.init(systemName: "ant.fill", withConfiguration: UIImage.SymbolConfiguration.init(pointSize: rect1.size.width * 1/2 * UIScreen.main.scale))?.withTintColor(bgColor, renderingMode: .alwaysOriginal)
            let w:CGFloat = rect1.width * 1/5
            let h = w * 1.2
            icon?.draw(in: CGRect.init(x: rect1.midX - w/2, y: rect1.midY - h/2, width: w, height: h))
        } else {
            // Fallback on earlier versions
        }
        
        let rect3 = CGRect.init(x: rect1.minX, y: rect1.maxY + sp, width: rect1.width, height: h)
        let bes3 = UIBezierPath(roundedRect: rect3, cornerRadius: corner)
        bes3.fill()
        
        let rect4 = CGRect.init(x: rect1.minX, y: rect3.maxY + sp, width: rect.width - sp * 2, height: h)
        let bes4 = UIBezierPath(roundedRect: rect4, cornerRadius: corner)
        bes4.fill()
        
        let rect2 = CGRect.init(x: rect1.maxX + sp, y: rect1.minY, width: rect.width - rect1.width - sp * 3, height: rect1.height + rect3.height + sp)
        let bes2 = UIBezierPath.init(roundedRect: rect2, cornerRadius: corner)
        bes2.fill()
    }
    
    
    @objc class func tastackPro(ctx:CGContext, size:CGSize) {
        if #available(iOS 13.0, *) {
            UIColor.white.setFill()
        } else {
            // Fallback on earlier versions
        }
        ctx.fill(.init(x: 0, y: 0, width: size.width, height: size.height))
        let dark = UIColor.init(white: 0.7, alpha: 1)
        mergeShadow(ctx: ctx)
        
        let s:CGFloat = 0.6
        let b:CGFloat = 1
        let sp:CGFloat = size.width/4
        let colors:[UIColor] = [
            .init(hue: 210/360, saturation: s, brightness: b, alpha: 1),
            .init(hue: 150/360, saturation: s, brightness: b, alpha: 1),
            .init(hue: 90/360, saturation: s, brightness: b, alpha: 1)
        ]
        
        var cs:[CGPoint] = []
        let y:CGFloat = size.height * 2.2/3
        for c in colors.enumerated() {
            cs.append(CGPoint.init(x: sp * CGFloat(c.offset + 1), y: y))
        }
        
        
        
        let mlw:CGFloat = size.width/80
        
        var rects:[CGRect] = []
        
        let mw:CGFloat = size.width * 1/5
        let radius:CGFloat = mw * 1.5/10
        func mRect(p:CGPoint, page:Int) {
            var w:CGFloat = mw
            if page == 0 {
                w *= 1.8
            }
            let rect:CGRect = .init(x: p.x - w/2, y: p.y - mw/2, width: w, height: mw)
            rects.append(rect)
        }
        
        let off:CGFloat = 1.3/4
        let yoff:CGFloat = off * 9/10
        mRect(p: .init(x: size.width/2, y: size.height * yoff), page: 0)
        mRect(p: .init(x: size.width * off, y: size.height * (1 - yoff)), page: 1)
        mRect(p: .init(x: size.width * (1 - off), y: size.height * (1 - yoff)), page: 2)
        
        
        
        func fill(page:Int) {
            let rect = rects[page]
            let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: radius)
            colors[page].setFill()
            //bes.lineWidth = mlw
            dark.setStroke()
            bes.stroke()
            bes.fill()
            
            //            if page == 0 {
            //                let pra = NSMutableParagraphStyle()
            //                pra.alignment = .center
            //                let ats = NSAttributedString.init(string: "H   S   T", attributes: [
            //                    NSAttributedString.Key.font : UIFont.systemFont(ofSize: rect.width * 1/6, weight: .bold),
            //                    NSAttributedString.Key.paragraphStyle : pra,
            //                    NSAttributedString.Key.foregroundColor : dark
            //                ])
            //                let s = ats.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size
            //                ats.draw(in: .init(x: rect.midX - s.width/2, y: rect.midY - s.height/2, width: s.width, height: s.height))
            //            }
        }
        
        
        //mergeShadow(ctx: ctx, color: UIColor.black)
        
        
        let lw:CGFloat = mlw/2 //2 * mlw//mlw/2
        let bes = UIBezierPath.init()
        bes.lineWidth = lw
        bes.move(to: .init(x: rects[1].midX, y: rects[1].minY))
        let c:CGPoint = .init(x: size.width/2, y: size.height/2)//.init(x: size.width/2, y: (rects[1].minY + rects[0].maxY)/2)
        bes.addQuadCurve(to: c, controlPoint: .init(x: rects[1].midX, y: c.y))
        
        bes.move(to: .init(x: rects[2].midX, y: rects[2].minY))
        bes.addQuadCurve(to: c, controlPoint: .init(x: rects[2].midX, y: c.y))
        
        bes.move(to: .init(x: c.x, y: rects[0].maxY))
        bes.addLine(to: c)
        
        dark.setStroke()
        bes.stroke()
        
        
        fill(page: 0)
        fill(page: 1)
        fill(page: 2)
        
        let tRect = rects[0]
        let pra = NSMutableParagraphStyle()
        pra.alignment = .center
        let txt = NSAttributedString.init(string: "H    S    T", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: tRect.width * 1/6, weight: .bold),
            NSAttributedString.Key.foregroundColor : UIColor.init(white: 0.3, alpha: 1),
            NSAttributedString.Key.paragraphStyle : pra
        ])
        let siz = txt.boundingRect(with: tRect.size, options: .usesLineFragmentOrigin, context: nil).size
        mergeShadow(ctx: ctx, color: UIColor.systemBlue)
        txt.draw(in: .init(x: tRect.minX, y: tRect.midY - siz.height/2, width: tRect.width, height: siz.height))
        ctx.saveGState()
        
    }
    
    
    @objc class func tastackUse(ctx:CGContext, size:CGSize) {
        let s:CGFloat = 0.6
        let b:CGFloat = 1
        let blue = UIColor.init(hue: 210/360, saturation: s, brightness: b, alpha: 1)
        let colors:[UIColor] = [
            .init(hue: 0/360, saturation: s, brightness: b, alpha: 1),
            .init(hue: 60/360, saturation: s, brightness: b, alpha: 1),
            .init(hue: 120/360, saturation: s, brightness: b, alpha: 1),
        ]
        
        
        UIColor.init(white: 0.3, alpha: 1).setFill()
        ctx.fill(.init(x: 0, y: 0, width: size.width, height: size.height))
        
        
        
        let off:CGFloat = size.width * 1/5
        let sp:CGFloat = size.width * 1/25
        
        let h = (size.height - off * 2 - sp) * 1.8/3
        let sh = (size.height - off * 2 - sp) * 1.2/3
        let xoff = off
        let tRect = CGRect.init(x: xoff, y: off, width: size.width - xoff * 2, height: h)
        let w:CGFloat = (tRect.width - sp * CGFloat(colors.count - 1))/CGFloat(colors.count)
        
        let clip = UIBezierPath.init(roundedRect: .init(x: tRect.minX, y: tRect.minY, width: tRect.width, height: tRect.height + sp + sh), cornerRadius: 30)
        clip.addClip()
        
        
        let cn:CGFloat = sp * 1.2/2
        
        func common(bes:UIBezierPath) {
            //            UIColor.white.setStroke()
            //            bes.lineWidth = size.width * 1/200
            //            bes.stroke()
        }
        
        
        //        UIColor.init(white: 0.4, alpha: 1).setFill()
        //        let of:CGFloat = sp * 3//size.width * 1/20
        //        let bs = UIBezierPath.init(roundedRect: .init(x: tRect.minX - of, y: tRect.minY - of, width: tRect.width + of * 2, height: size.height - off * 2 + of * 2), cornerRadius: cn)
        //        bs.fill()
        
        blue.setFill()
        
        let bes = UIBezierPath.init(roundedRect: tRect, cornerRadius: cn)
        bes.fill()
        common(bes: bes)
        
        for color in colors.enumerated() {
            let rect = CGRect.init(x: CGFloat(color.offset) * (w + sp) + tRect.minX, y: tRect.maxY + sp, width: w, height: sh)
            let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: cn)
            color.element.setFill()
            bes.fill()
            common(bes: bes)
        }
        
        let pra = NSMutableParagraphStyle()
        pra.alignment = .center
        let txt = NSAttributedString.init(string: "T    S    H", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 50, weight: .bold),
            NSAttributedString.Key.foregroundColor : UIColor.yellow,
            NSAttributedString.Key.paragraphStyle : pra
        ])
        let siz = txt.boundingRect(with: tRect.size, options: .usesLineFragmentOrigin, context: nil).size
        mergeShadow(ctx: ctx, color: UIColor.yellow)
        txt.draw(in: .init(x: tRect.minX, y: tRect.midY - siz.height/2, width: tRect.width, height: siz.height))
        ctx.saveGState()
        
    }
    
    //0.85
    class func mergeShadow(ctx:CGContext, color:UIColor = UIColor.init(white: 0.8, alpha: 1), radius:CGFloat = 20) {
        let shadowOffset = CGSize(width: 0, height: 0)
        //此函数创建和应用阴影
        ctx.setShadow(offset: shadowOffset, blur: radius, color: color.cgColor)
    }
    
    
    /**
     * process1
     */
    @objc class func process1(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(10)
        UIColor.init(white: 0.2, alpha: 1).setFill()
        bes.fill()
        let path = CGMutablePath()
        let off:CGFloat = 2.35
        let points = (CGPoint.init(x: size.width * 1/4, y: size.height * 1/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 1/4),
                      
                      CGPoint.init(x: size.width * 1/4, y: size.height * 3/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 3/4),
                      
                      CGPoint.init(x: size.width/2, y: size.height/off),
                      CGPoint.init(x: size.width/2, y: size.height * (1 - 1/off)))
        
        path.move(to: points.0)
        path.addLine(to: points.1)
        path.addLine(to: points.2)
        path.addLine(to: points.3)
        
        path.move(to: points.4)
        path.addLine(to: points.5)
        path.addLine(to: points.6)
        path.addLine(to: points.7)
        
        path.move(to: points.8)
        path.addLine(to: points.9)
        
        UIColor.systemGray4.setStroke()
        ctx.addPath(path)
        ctx.strokePath()
        
        
        let ps = [points.0, points.3, points.4, points.7, points.8, points.9]
        for point in ps {
            let w:CGFloat = 40
            let rect = CGRect.init(x: point.x - w/2, y: point.y - w/2, width: w, height: w)
            ctx.addEllipse(in: rect)
        }
        UIColor.systemGray6.setFill()
        ctx.fillPath()
    }
    
    /**
     * nodeNote
     */
    @objc class func nodeNote(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        //UIColor.init(white: 0.15, alpha: 1).setFill()
        UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1).setFill()
        bes.fill()
        ctx.setLineWidth(10)
        mergeShadow(ctx: ctx, color: UIColor.init(white: 0.85, alpha: 1))
        let path = CGMutablePath()
        let off:CGFloat = 2.35
        let points = (CGPoint.init(x: size.width * 1/4, y: size.height * 1/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 1/4),
                      
                      CGPoint.init(x: size.width * 1/4, y: size.height * 3/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 3/4),
                      
                      CGPoint.init(x: size.width/2, y: size.height/off),
                      CGPoint.init(x: size.width/2, y: size.height * (1 - 1/off)))
        
        path.move(to: points.0)
        path.addLine(to: points.1)
        path.addLine(to: points.2)
        path.addLine(to: points.3)
        
        path.move(to: points.4)
        path.addLine(to: points.5)
        path.addLine(to: points.6)
        path.addLine(to: points.7)
        
        path.move(to: points.8)
        path.addLine(to: points.9)
        
        //UIColor.systemGray4.setStroke()
        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 0.6).setStroke()
        UIColor(white: 1, alpha: 0.6).setStroke()
        ctx.addPath(path)
        ctx.strokePath()
        
        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1).setFill()
        //UIColor(white: 1, alpha: 1).setFill()
        UIColor.white.setFill()
        
        let ps = [points.0, points.3, points.4, points.7, points.8, points.9]
        for point in ps {
            let w:CGFloat = 40
            let rect = CGRect.init(x: point.x - w/2, y: point.y - w/2, width: w, height: w)
            let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: w * 1/2)
            bes.fill()
            //ctx.addEllipse(in: rect)
        }
        
        //UIColor.systemGray6.setFill()
        // ctx.fillPath()
    }
    
    /**
     * nodeNote
     */
    @objc class func mindNote(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        //UIColor.init(white: 0.15, alpha: 1).setFill()
        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1).setFill()
        UIColor.init(white: 0.13, alpha: 1).setFill()
        bes.fill()
        ctx.setLineWidth(10)
        mergeShadow(ctx: ctx)
        let path = CGMutablePath()
        let off:CGFloat = 2.35
        let points = (CGPoint.init(x: size.width * 1/4, y: size.height * 1/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height/off),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 1/4),
                      
                      CGPoint.init(x: size.width * 1/4, y: size.height * 3/4),
                      CGPoint.init(x: size.width * 1/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * (1 - 1/off)),
                      CGPoint.init(x: size.width * 3/4, y: size.height * 3/4),
                      
                      CGPoint.init(x: size.width/2, y: size.height/off),
                      CGPoint.init(x: size.width/2, y: size.height * (1 - 1/off)))
        
        path.move(to: points.0)
        path.addLine(to: points.1)
        path.addLine(to: points.2)
        path.addLine(to: points.3)
        
        path.move(to: points.4)
        path.addLine(to: points.5)
        path.addLine(to: points.6)
        path.addLine(to: points.7)
        
        path.move(to: points.8)
        path.addLine(to: points.9)
        
        //UIColor.systemGray4.setStroke()
        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 0.6).setStroke()
        UIColor(white: 1, alpha: 0.6).setStroke()
        ctx.addPath(path)
        ctx.strokePath()
        
        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1).setFill()
        UIColor(white: 1, alpha: 1).setFill()
        //UIColor.init(red: 228/255, green: 199/255, blue: 255/255, alpha: 1).setFill()
        
        
        let ps = [points.0, points.3, points.4, points.7, points.8, points.9]
        for point in ps {
            let w:CGFloat = 40
            let rect = CGRect.init(x: point.x - w/2, y: point.y - w/2, width: w, height: w)
            let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: w * 1/2)
            bes.fill()
            //ctx.addEllipse(in: rect)
        }
        
        //UIColor.systemGray6.setFill()
        // ctx.fillPath()
    }
    
    
    
    
    
    
    /**
     * focustack
     */
    @objc class func focustack(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        //UIColor.init(white: 0.15, alpha: 1).setFill()
        UIColor.white.setFill()
        bes.fill()
        let color = UIColor.xbpuple
        color.setFill()
        bes.fill()
        //ctx.setLineWidth(10)
        mergeShadow(ctx: ctx)
        
        let off:CGFloat = size.width * 1/5
        let rec = CGRect.init(x: off, y: off * 1.2, width: size.width - off * 2, height: size.height - off * 2.4)
        //        let besQube = UIBezierPath.init(roundedRect: rec, cornerRadius: off * 1/5)
        //        UIColor.init(white: 1, alpha: 0.5).setFill()
        //        besQube.fill()
        
        ctx.setLineWidth(rec.height * 1/8)
        ctx.setLineCap(.round)
        
        let y:CGFloat = rec.minY //+ rec.height * 1/6
        ctx.move(to: .init(x: rec.minX + rec.width * 1/8, y: y))
        ctx.addLine(to: .init(x: rec.maxX - rec.width * 1/8, y: y))
        UIColor.init(white: 1, alpha: 0.8).setStroke()
        ctx.strokePath()
        
        ctx.setLineWidth(rec.height * 1/8 - 20)
        ctx.move(to: .init(x: rec.minX + rec.width * 4/8, y: y))
        ctx.addLine(to: .init(x: rec.maxX - rec.width * 1/8 - 3, y: y))
        //UIColor.init(white: 1, alpha: 0.7).setStroke()
        UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 0.7).setStroke()
        ctx.strokePath()
        
        //"square.2.stack.3d.top.fill" // //"square.2.stack.3d.top.fill"
        let image = UIImage.init(systemName: "square.fill.on.square", withConfiguration: UIImage.SymbolConfiguration.init(pointSize: size.width * 1/4))?.withTintColor(.init(white: 1, alpha: 0.8), renderingMode: .alwaysOriginal)
        let s = image!.size
        image?.draw(in: .init(x: size.width/2 - s.width/2, y: rec.maxY - rec.height * 1/3.5 - s.height/2, width: s.width, height: s.height))
        
        //        let path = CGMutablePath()
        //        let off:CGFloat = 2.35
        //        let points = (CGPoint.init(x: size.width * 1/4, y: size.height * 1/4),
        //                      CGPoint.init(x: size.width * 1/4, y: size.height/off),
        //                      CGPoint.init(x: size.width * 3/4, y: size.height/off),
        //                      CGPoint.init(x: size.width * 3/4, y: size.height * 1/4),
        //
        //                      CGPoint.init(x: size.width * 1/4, y: size.height * 3/4),
        //                      CGPoint.init(x: size.width * 1/4, y: size.height * (1 - 1/off)),
        //                      CGPoint.init(x: size.width * 3/4, y: size.height * (1 - 1/off)),
        //                      CGPoint.init(x: size.width * 3/4, y: size.height * 3/4),
        //
        //                      CGPoint.init(x: size.width/2, y: size.height/off),
        //                      CGPoint.init(x: size.width/2, y: size.height * (1 - 1/off)))
        //
        //        path.move(to: points.0)
        //        path.addLine(to: points.1)
        //        path.addLine(to: points.2)
        //        path.addLine(to: points.3)
        //
        //        path.move(to: points.4)
        //        path.addLine(to: points.5)
        //        path.addLine(to: points.6)
        //        path.addLine(to: points.7)
        //
        //        path.move(to: points.8)
        //        path.addLine(to: points.9)
        //
        //        //UIColor.systemGray4.setStroke()
        //        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 0.6).setStroke()
        //        UIColor(white: 1, alpha: 0.6).setStroke()
        //        ctx.addPath(path)
        //        ctx.strokePath()
        //
        //        //UIColor.init(red: 148/255, green: 119/255, blue: 212/255, alpha: 1).setFill()
        //        UIColor(white: 1, alpha: 1).setFill()
        //
        //        let ps = [points.0, points.3, points.4, points.7, points.8, points.9]
        //        for point in ps {
        //            let w:CGFloat = 40
        //            let rect = CGRect.init(x: point.x - w/2, y: point.y - w/2, width: w, height: w)
        //            let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: w * 1/2)
        //            bes.fill()
        //            //ctx.addEllipse(in: rect)
        //        }
        //
        //UIColor.systemGray6.setFill()
        // ctx.fillPath()
    }
    
    
    /**
     * sorter
     */
    @objc class func sorter(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        //UIColor.init(white: 0.15, alpha: 1).setFill()
        UIColor.white.setFill()
        bes.fill()
        let color = UIColor.xbpuple
        color.setFill()
        bes.fill()
        //mergeShadow(ctx: ctx)
        
        let off:CGFloat = size.width * 1/5
        let yoff = off * 1.1
        let sp:CGFloat = size.width * 1/25
        let r1 = CGRect.init(x: off * 2/3, y: yoff, width: size.width * 1/3 + off/2.2, height: size.height - yoff * 2)
        let count:CGFloat = 4
        let h = (r1.height - sp * (count - 1))/count
        let w = size.width - r1.maxX - sp - yoff
        var rs:[CGRect] = []
        for i in 0..<Int(count) {
            rs.append(.init(x: r1.maxX + sp * 1.7, y: yoff + (sp + h) * CGFloat(i), width: w, height: h))
        }
        //rs.append(r1)
        
        let c:CGFloat = size.width * 1/30
        for r in rs.enumerated() {
            let bes = UIBezierPath.init(roundedRect: r.element, cornerRadius: c * 0.6) //r.offset == rs.count - 1 ? c :
            UIColor.init(white: 1, alpha: r.offset % 2 == 0 ? 0.3 : 1).setFill()
            bes.fill()
        }
        
        UIColor.init(white: 1, alpha: 1).setFill()
        let image = UIImage.init(systemName: "square.2.stack.3d.top.filled", withConfiguration: UIImage.SymbolConfiguration(pointSize: size.width * 1/1.8 * UIScreen.main.scale))?.withTintColor(.white, renderingMode: .alwaysOriginal)
        image?.draw(in: r1.fitRect(siz: image!.size, off: 0))
    }
    
    
    @objc class func icliper(ctx:CGContext, size:CGSize) {
        UIColor.init(hue: 120/360, saturation: 0.5, brightness: 0.7, alpha: 1).setFill()
        ctx.fill(.init(x: 0, y: 0, width: size.width, height: size.height))
        ctx.move(to: .init(x: size.width * 1/3, y: size.height * 1/3))
        ctx.addLine(to: .init(x: size.width * 1/3, y: size.height * 2/3))
        ctx.addLine(to: .init(x: size.width * 2/3, y: size.height * 2/3))
        ctx.setLineWidth(size.width * 1/25)
        UIColor.white.setStroke()
        ctx.strokePath()
        
        UIColor.white.setFill()
        let w = size.width * 1/8
        ctx.fill(.init(x: size.width * 2/3 - w, y: size.height * 1/3, width: w, height: w))
        
    }
    
    
    @objc class func xcolorPro(ctx:CGContext, size:CGSize) {
        let bes = UIBezierPath.init(roundedRect: .init(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.xbpuple.setFill()//UIColor.init(white: 0.17, alpha: 1).setFill()
        
        let s:CGFloat = 0.6
        //UIColor.init(hue: 210/360, saturation: s, brightness: 0.2, alpha: 1).setFill()
        bes.fill()
        let xc:Int = 3
        let c:Int = 2
        let count:Int = c * xc
        let w = size.width * 1/2.2
        
        let sp:CGFloat = size.width * 1/40
        let mw:CGFloat = (w - sp * CGFloat(xc - 1))/CGFloat(xc)
        
        let off:CGFloat = size.width/2 - w/2 //+ sp
        let myoff:CGFloat = size.height/2 - (w * 2/3)/2 //+ sp
        let dis:CGFloat = 80
        
        var ph:CGFloat = 0
        UIColor.xbpuple.getHue(&ph, saturation: nil, brightness: nil, alpha: nil)
        ph *= 360
        ph -= 180
        
        let cent:CGFloat = 35
        var coff:CGFloat = cent - dis/2
        var out:CGFloat = cent + 180
        if out > 360 {
            out -= 360
        }
        if coff < 360 {
            coff += 360
        }
        let csp:CGFloat = dis/CGFloat(count - 1)
        
        
        var colors:[UIColor] = []
        var rects:[CGRect] = []
        
        
        for i in 0..<count {
            var h:CGFloat = (coff + csp * CGFloat(i))/360
            if h > 1 {
                h -= 1
            }
            print(h * 360)
            let color = UIColor.init(hue: h, saturation: s, brightness: 1, alpha: 0.9)
            colors.append(color)
            
            let rect = CGRect.init(x: off + CGFloat(i % xc) * (mw + sp), y: myoff + (mw + sp) * CGFloat(i/xc), width: mw, height: mw)
            rects.append(rect)
        }
        
        let (first, last) = (rects[0], rects[count - 1])
        
        
        for i in 0..<count {
            let mbes = UIBezierPath.init(roundedRect: rects[i], cornerRadius: size.height * 1/50)
            //let page = (i % 2) == 0 ? (count - i - 1) : i
            colors[i].setFill()
            mbes.fill()
        }
        //mergeShadow(ctx: ctx, color: .init(white: 0.55, alpha: 1))
        
        if let ctx = UIGraphicsGetCurrentContext() {
            mergeShadow(ctx: ctx)
        }
        
        let pw:CGFloat = size.width * 1/18
        let of:CGFloat = size.width * 1/6
        let point = UIBezierPath.init(ovalIn: .init(x: size.width - pw - of, y: of, width: pw, height: pw))
        UIColor.white.setFill()
        point.fill()
        
//        let pointOff = size.width * 1/10
//        let pointW:CGFloat = size.width * 1/15
//        ctx.addEllipse(in: .init(x: first.minX - pointOff, y: first.minY - pointOff * 3/2, width: pointW, height: pointW))
//        UIColor.white.setFill()
//        ctx.fillPath()
    }
    
    
    @objc class func colorCo(ctx:CGContext, size:CGSize) {
        let bes = UIBezierPath.init(roundedRect: .init(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(hue: 210/360, saturation: 0.9, brightness: 0.7, alpha: 1).setFill()
        bes.fill()
        let count:Int = 3
        let w = size.width * 1/3.5
        
        let sp:CGFloat = size.width * 1/100
        let mw:CGFloat = (w - sp * CGFloat(count))/CGFloat(count + 1)
        
        let off:CGFloat = size.width/2 - w/2
        let s:CGFloat = 0.6
        let coff:CGFloat = 15
        let csp:CGFloat = 15
        
        
        var colors:[UIColor] = []
        var rects:[CGRect] = []
        for i in 0...count {
            var h:CGFloat = (coff + csp * CGFloat(i))/360
            if h > 1 {
                h -= 1
            }
            let color = UIColor.init(hue: h, saturation: s, brightness: 1, alpha: 1)
            colors.append(color)
            
            let rect = CGRect.init(x: off + CGFloat(i) * (mw + sp), y: size.height/2 - w/2, width: mw, height: w)
            rects.append(rect)
        }
        
        let (first, last) = (rects[0], rects[count])
        
        
        ctx.setLineWidth(size.height * 1/100)
        let yoff:CGFloat = 1/10
        let offsp = yoff * size.height
        var y = last.minY - size.height * yoff
        ctx.move(to: .init(x: 0, y: y))
        ctx.addLine(to: .init(x: last.maxX + offsp, y: y))
        colors[3].setStroke()
        ctx.strokePath()
        
        y = first.maxY + size.height * yoff
        ctx.move(to: .init(x: size.width, y: y))
        ctx.addLine(to: .init(x: first.minX - offsp, y: y))
        
        colors[0].setStroke()
        ctx.strokePath()
        
        
        ctx.move(to: .init(x: first.minX - yoff * size.height, y: first.minY - offsp))
        ctx.addLine(to: .init(x: first.minX - yoff * size.height, y: size.height))
        colors[2].setStroke()
        ctx.strokePath()
        
        ctx.move(to: .init(x: last.maxX + yoff * size.height, y: last.maxY + offsp))
        ctx.addLine(to: .init(x: last.maxX + yoff * size.height, y: 0))
        colors[1].setStroke()
        ctx.strokePath()
        
        ctx.setLineWidth(0)
        
        //        let mbes = UIBezierPath.init(roundedRect: CGRect.init(x: off, y: size.height/2 - w/2, width: size.width - off * 2, height: w), cornerRadius: size.height * 1/30)
        //        mbes.addClip()
        
        for i in 0..<count {
            let mbes = UIBezierPath.init(roundedRect: rects[i], cornerRadius: size.height * 1/80)
            //let page = (i % 2) == 0 ? (count - i - 1) : i
            colors[i].setFill()
            mbes.fill()
        }
    }
    
    class func offColors(color:UIColor = .xbpuple) -> [UIColor] {
        var ph:CGFloat = 0
        color.getHue(&ph, saturation: nil, brightness: nil, alpha: nil)
        ph *= 360
        ph -= 180
        
        let count:Int = 6
        
        let dis:CGFloat = 90
        let cent:CGFloat = 30
        var coff:CGFloat = cent - dis/2
        var out:CGFloat = cent + 180
        if out > 360 {
            out -= 360
        }
        if coff < 360 {
            coff += 360
        }
        let csp:CGFloat = dis/CGFloat(count - 1)
        var colors:[UIColor] = []
        let s:CGFloat = 0.6
       
        for i in 0..<count {
            var h:CGFloat = (coff + csp * CGFloat(i))/360
            if h > 1 {
                h -= 1
            }
            print(h * 360)
            let color = UIColor.init(hue: h, saturation: s, brightness: 1, alpha: 0.9)
            colors.append(color)
        }
        
        return colors
    }
    
    @objc class func colorMaxFlower(ctx:CGContext, size:CGSize) {
        let bes = UIBezierPath.init(roundedRect: .init(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.xbpuple.setFill()
        bes.fill()
        
        let w:CGFloat = size.width * 1/18
       
        
        let radius:CGFloat = 25//size.width/2 - 200
        let bigRadius:CGFloat = size.width * 1/2.5
        
        let colors = offColors()
  
        let sp:CGFloat = .pi * 2 / CGFloat(colors.count)
        let off = -sp/2
        let cent = CGPoint.init(x: size.width/2, y: size.height/2)
        
        var locs:[CGPoint] = []
        
        for obj in colors.enumerated() {
            let angle1 = off + sp * CGFloat(obj.offset)
            let angle2 = angle1 + sp
            let pStart = Math.arcCoord(cent: cent, r: radius, angle: angle1)
            let pEnd = Math.arcCoord(cent: cent, r: radius, angle: angle2)
            
            let bes = UIBezierPath.init()
            
            let aoff:CGFloat = 0//.pi/15
            let pControl1 = Math.arcCoord(cent: cent, r: bigRadius, angle: angle1 - aoff)
            let pControl2 = Math.arcCoord(cent: cent, r: bigRadius, angle: angle2 + aoff)
            bes.move(to: pStart)
            bes.addCurve(to: pEnd, controlPoint1: pControl1, controlPoint2: pControl2)
            
            bes.close()
            obj.element.setFill()
            bes.fill()
    
            let p = Math.arcCoord(cent: cent, r: radius + 30, angle: angle1 + sp/2)
            locs.append(p)
        }
        
        
    
        func fillCircle(color:UIColor, mw:CGFloat) {
            let cp = UIBezierPath.init(ovalIn: .init(x: size.width/2 - mw/2, y: size.height/2 - mw/2, width: mw, height: mw))
            color.setFill()
            cp.fill()
        }
        fillCircle(color: .xbpuple, mw: size.width * 1/9)
        mergeShadow(ctx: ctx)
        fillCircle(color: .yellow, mw: w)
        
        let of:CGFloat = size.width * 1/6
        let point = UIBezierPath.init(ovalIn: .init(x: size.width - w - of, y: of, width: w, height: w))
        UIColor.white.setFill()
        point.fill()
        
        
    }
    
    @objc class func colorDesk(ctx:CGContext, size:CGSize) {
        let bes = UIBezierPath.init(roundedRect: .init(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(hue: 210/360, saturation: 0.9, brightness: 0.7, alpha: 1).setFill()
        bes.fill()
        let c:Int = 3
        let count:Int = c * c
        let w = size.width * 1/3.5
        
        let sp:CGFloat = size.width * 1/120
        let mw:CGFloat = (w - sp * CGFloat(c - 1))/CGFloat(c)
        
        let off:CGFloat = size.width/2 - w/2
        let myoff:CGFloat = size.height/2 - w/2
        let s:CGFloat = 0.65
        let dis:CGFloat = 120
        var coff:CGFloat = 30 - dis/2
        if coff < 360 {
            coff += 360
        }
        let csp:CGFloat = dis/CGFloat(count - 1)
       
    
        var colors:[UIColor] = []
        var rects:[CGRect] = []
        for i in 0..<count {
            var h:CGFloat = (coff + csp * CGFloat(i))/360
            if h > 1 {
                h -= 1
            }
            print(h * 360)
            let color = UIColor.init(hue: h, saturation: s, brightness: 1, alpha: 1)
            colors.append(color)
            
            let rect = CGRect.init(x: off + CGFloat(i % c) * (mw + sp), y: myoff + (mw + sp) * CGFloat(i/c), width: mw, height: mw)
            rects.append(rect)
        }
        
        let (first, last) = (rects[0], rects[count - 1])
        
        
        var lcs:[UIColor] = []
        let lsp:CGFloat = 40
        let ls:CGFloat = lsp/3
        for i in 0...3 {
            var mh:CGFloat = 30 - lsp/2 + CGFloat(i) * ls
            if mh > 360 {
                mh -= 360
            }
            lcs.append(.init(hue: 210/360, saturation: 0.6, brightness: 1, alpha: 1))
        }
        
        ctx.setLineWidth(size.width * 1/60)
        let yoff:CGFloat = 1/10
        let offsp = yoff * size.height
        var y = first.minY - size.height * yoff
        ctx.move(to: .init(x: 0, y: y))
        ctx.addLine(to: .init(x: last.maxX + offsp, y: y))
        lcs[0].setStroke()
        ctx.strokePath()

        
        ctx.move(to: .init(x: first.minX - yoff * size.height, y: first.minY - offsp))
        ctx.addLine(to: .init(x: first.minX - yoff * size.height, y: size.height))
        lcs[1].setStroke()
        ctx.strokePath()

        y = last.maxY + size.height * yoff
        ctx.move(to: .init(x: size.width, y: y))
        ctx.addLine(to: .init(x: first.minX - offsp, y: y))
        lcs[2].setStroke()
        ctx.strokePath()

        ctx.move(to: .init(x: last.maxX + yoff * size.height, y: last.maxY + offsp))
        ctx.addLine(to: .init(x: last.maxX + yoff * size.height, y: 0))
        lcs[3].setStroke()
        ctx.strokePath()

        ctx.setLineWidth(0)
        
        
        //CGRect.init(x: off, y: size.height/2 - w/2, width: size.width - off * 2, height: w)
        for i in 0..<count {
            let mbes = UIBezierPath.init(roundedRect: rects[i], cornerRadius: size.height * 1/80)
            //let page = (i % 2) == 0 ? (count - i - 1) : i
            colors[i].setFill()
            mbes.fill()
        }
    }
    
    @objc class func draft(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        let color = UIColor.init(white: 0.15, alpha: 1)
        UIColor.white.setFill()
        bes.fill()
        
        var w:CGFloat = size.width * 1/1.5
        let img = UIImage.init(systemName: "book.closed.fill", withConfiguration: UIImage.SymbolConfiguration.init(font: .systemFont(ofSize: w, weight: .thin)))?.withRenderingMode(.alwaysOriginal).withTintColor(color)
        img?.draw(in: .init(x: size.width/2 - w/2, y: size.height/2 - w/2, width: w, height: w))

        //star.square.fill
        w = size.width * 1/5
        let icon = UIImage.init(systemName: "star.square.fill", withConfiguration: UIImage.SymbolConfiguration.init(font: .systemFont(ofSize: w, weight: .thin)))?.withRenderingMode(.alwaysOriginal).withTintColor(.white)
        let w_h = icon!.size.width/icon!.size.height
        let h = w / w_h
        let x = size.width/1.85 - w/2
        let y = size.height/2.31 - h/2
        icon?.draw(in: .init(x: x, y: y, width: w, height: h))
    }
    
    
    
    @objc class func xcolor(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(white: 0.15, alpha: 1).setFill()
        bes.fill()
        
        let sp:CGFloat = size.height * 1/10
        let sm = sp// * 4/5
        
    
        func color(arc:CGFloat) -> UIColor {
            var arc = arc
            if arc < 0 {
                arc += .pi * 2
            }
            if arc > .pi * 2 {
                arc -= .pi * 2
            }
            return UIColor.init(hue: arc/(.pi * 2), saturation: 0.65, brightness: 1, alpha: 1)
        }
        

        let sw = size.width * 1/18
        ctx.addEllipse(in: CGRect.init(x: size.width * 5/6 - sw, y: size.height * 1/6, width: sw, height: sw))
        UIColor.systemOrange.setFill()
        ctx.fillPath()
        
        let offb = size.height * 1/2 + sp - sm/2
        let offa = size.height * 1/2 - sp - sm/2
        
        let rec = CGRect.init(x: size.width * 1.2/4, y: size.height * 1/2 - sp - sm/2,
                              width: size.width * (1 - 1.2/4 * 2), height: offb - offa + sm)
        
        let mbes = UIBezierPath.init(roundedRect: rec, cornerRadius: sm/2)
        mbes.addClip()
        

        let compoents:[CGFloat] = [ 0.7, 1, 0, 1,
                                    1, 0.4, 0.2, 1,
                                    0.2, 0.5, 1, 1]
        
        let locations:[CGFloat] = [0, 1/2, 1]
        let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(), colorComponents: compoents, locations: locations, count: locations.count)
        /*
         绘制线性渐变
         context:图形上下文
         gradient:渐变色
         startPoint:起始位置
         endPoint:终止位置
         options:绘制方式,DrawsBeforeStartLocation 开始位置之前就进行绘制，到结束位置之后不再绘制，
         DrawsAfterEndLocation开始位置之前不进行绘制，到结束点之后继续填充
         */
        ctx.drawLinearGradient(gradient!,
                               start: CGPoint.init(x: rec.midX, y: rec.minY),
                               end: CGPoint.init(x: rec.midX, y: rec.maxY), options: CGGradientDrawingOptions.drawsAfterEndLocation)
    
    }
    
    @objc class func imp(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        kcolor(56, 56, 56).setFill()
        bes.fill()
        
        let w:CGFloat = 0
        ctx.setLineWidth(w)
        
        let sw = size.width * 1/18
        ctx.addEllipse(in: CGRect.init(x: size.width * 5/6 - sw, y: size.height * 1/6, width: sw, height: sw))
        
        
        let left:CGFloat = 1.3/4
        let top:CGFloat = 1.5/5
        ctx.addRect(CGRect.init(x: size.width * left, y: size.height * top, width: size.width * (1 - 2*left), height: size.height * (1-2*top)))
        kcolor(255, 100, 100).setFill()
        ctx.fillPath()
        
        let lines = [
            (size.width * left, size.height * top),
            (size.width * 3.5/8, size.height * 1.8/3),
            (size.width * 4.5/8, size.height * (3 - 1.8)/3),
            (size.width * (1-left), size.height * (1-top)),
            (size.width * (1-left), size.height * top)
        ]
        
        
        for it in lines.enumerated() {
            if it.offset == 0 {
                ctx.move(to: CGPoint.init(x: it.element.0, y: it.element.1))
            }
            ctx.addLine(to: CGPoint.init(x: it.element.0, y: it.element.1))
        }
        ctx.closePath()
        
        
        
        kcolor(255, 255, 100).setFill()
        ctx.fillPath()
    }
    
    @objc class func xPhoto(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)

        UIColor.xbpuple.setFill()
        bes.fill()

        
        if let ctx = UIGraphicsGetCurrentContext() {
            mergeShadow(ctx: ctx)
        }
        let pw:CGFloat = size.width * 1/18
        let of:CGFloat = size.width * 1/6
        let point = UIBezierPath.init(ovalIn: .init(x: size.width - pw - of, y: of, width: pw, height: pw))
        UIColor.white.setFill()
        point.fill()
        
        let moff:CGFloat = size.width * 1/4
        func drawAct() {
            let w:CGFloat = size.width * 1/7.5
            let off:CGFloat = moff
            let points:[(CGFloat, CGFloat)] = [
                (size.width/2, off),
                (size.width/2 - w/2, size.width/2 - w/2),
                (off, size.height/2),
                (size.width/2 - w/2, size.height/2 + w/2),
                (size.width/2, size.height - off),
                (size.width/2 + w/2, size.height/2 + w/2),
                (size.width - off, size.height/2),
                (size.width/2 + w/2, size.height/2 - w/2),
                (size.width/2, off)
            ]
            let moff:CGFloat = 1/8
            for itm in points.enumerated() {
                if itm.offset == 0 {
                    ctx.move(to: CGPoint.init(x: itm.element.0 - size.width * moff * 0.5, y: itm.element.1 - size.width * moff * 0.5))
                } else {
                    ctx.addLine(to: CGPoint.init(x: itm.element.0 - size.width * moff * 0.5, y: itm.element.1 - size.width * moff * 0.5))
                }
            }
            ctx.closePath()
        
            ctx.clip()
            drawLinearGradient(context: ctx, start: .init(x: moff, y: moff), end: CGPoint.init(x: size.width * 3/4, y: size.height * 3/4))
        }
        drawAct()
    }
    
    /**
     绘制线性渐变
     
     - parameter context: 上下文
     */
    class func drawLinearGradient(context:CGContext, start:CGPoint, end:CGPoint){
        
        //使用rgb颜色空间
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        /*
         指定渐变色
         space:颜色空间
         components:颜色数组,注意由于指定了RGB颜色空间，那么四个数组元素表示一个颜色（red、green、blue、alpha），
         如果有三个颜色则这个数组有4*3个元素
         locations:颜色所在位置（范围0~1），这个数组的个数不小于components中存放颜色的个数
         count:渐变个数，等于locations的个数
         */
        var compoents:[CGFloat] = [ 0.3, 0.3, 0, 1,
                                    0.8, 1.0, 1.0, 1,
                                    0.4, 0.4, 0.35, 1,]
        compoents = [
            1, 1, 0, 1,
            1, 0.6, 0.6, 1,
            1, 1, 0.8, 1//0.3, 0.3, 1.0, 1,
        ]
        
        let locations:[CGFloat] = [1/4,2/4, 3/4]
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: compoents, locations: locations, count: locations.count)
        /*
         绘制线性渐变
         context:图形上下文
         gradient:渐变色
         startPoint:起始位置
         endPoint:终止位置
         options:绘制方式,DrawsBeforeStartLocation 开始位置之前就进行绘制，到结束位置之后不再绘制，
         DrawsAfterEndLocation开始位置之前不进行绘制，到结束点之后继续填充
         */
        context.drawLinearGradient(gradient!, start: start, end: end, options: CGGradientDrawingOptions.drawsAfterEndLocation)
    }
    
    class func randomFloat(off:UInt32)->CGFloat {
        return CGFloat((arc4random() % (256 - off) + off))
    }
    
    
    class func randomColor() -> UIColor {
        return kcolor(randomFloat(off: 80), randomFloat(off: 80), randomFloat(off: 80))
    }
    
    
    @objc class func vision(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        kcolor(56, 56, 56).setFill()
        bes.fill()
        ctx.setLineWidth(3 * UIScreen.main.scale)
        
        
        func lightColor() -> UIColor {
            return randomColor()
        }
        
        
        
        func strokOut() {
            let cent = CGPoint.init(x: size.width/2, y: size.height/2)
            let poins = arc_points(loc: CGPoint.init(x: size.width/2, y: size.height * 1/5), center: cent, num: 8)
            
            for i in 0...poins.count-1 {
                let loc = poins[i]
                ctx.move(to: loc)
                ctx.addLine(to: cent)
                ctx.setLineCap(.round)
                ctx.setLineWidth(2 * UIScreen.main.scale)
                lightColor().setStroke()
                ctx.strokePath()
                
                ctx.setLineWidth(0)
                let sw:CGFloat = 11
                ctx.addEllipse(in: CGRect.init(x: loc.x - sw/2 * UIScreen.main.scale,
                                               y: loc.y - sw/2 * UIScreen.main.scale,
                                               width: sw * UIScreen.main.scale, height: sw * UIScreen.main.scale))
                lightColor().setFill()
                ctx.fillPath()
            }
        }
        strokOut()
        
//        let sw = size.width * 1/18
//        ctx.addEllipse(in: CGRect.init(x: size.width * 5/6 - sw, y: size.height * 1/6, width: sw, height: sw))
//        kcolor(255, 100, 100).setFill()
//        ctx.fillPath()
        
    }
    
    
    /**
     * YSool
     */
    @objc class func iconCtxHandle(ctx:CGContext, size:CGSize) {
        let rect = CGRect.init(origin: CGPoint.zero, size: size)
        let bes = UIBezierPath.init(roundedRect: rect, cornerRadius: 0) //size.width/8
        ctx.setLineWidth(0)
        UIColor.init(white: 0.2, alpha: 1).setFill()
        bes.fill()
        
        let xpad:CGFloat = size.width * 1/6
        let ypad:CGFloat = size.height * 1/3
        let w:CGFloat = size.width * 1/18
        let fw:CGFloat = (size.height - ypad * 2)/2
        let sp:CGFloat = fw/3
        ctx.setLineCap(.round)
        ctx.setLineWidth(w)
        
        
        let r = fw * 2/2.2
        ctx.move(to: CGPoint.init(x: xpad + fw/2 - r/2, y: ypad))
        ctx.addArc(center: CGPoint.init(x: xpad + fw/2, y: ypad), radius: r/2, startAngle: CGFloat.pi, endAngle: 0, clockwise: true)
//        ctx.addQuadCurve(to: CGPoint.init(x: xpad + fw/2, y: ypad + fw),
//                         control: CGPoint.init(x: xpad + fw/6, y: ypad + fw))
//        ctx.move(to: CGPoint.init(x: xpad + fw, y: ypad))
//        ctx.addQuadCurve(to: CGPoint.init(x: xpad + fw/2, y: ypad + fw),
//                         control: CGPoint.init(x: xpad + fw * 5/6, y: ypad + fw))
        
        ctx.move(to: CGPoint.init(x: xpad + fw/2, y: ypad + r/2))
        ctx.addLine(to: CGPoint.init(x: xpad + fw/2, y: size.height - ypad))
        
        blue.setStroke()
        
        
        ctx.move(to: CGPoint.init(x: xpad + fw + fw * 3, y: ypad))
//        ctx.addArc(center: CGPoint.init(x: xpad + fw + sp + fw/2, y: ypad + fw/2), radius: fw/2, startAngle: CGFloat.pi * 3/2, endAngle: CGFloat.pi/2, clockwise: true)
//        ctx.addArc(center: CGPoint.init(x: xpad + fw + sp + fw/2, y: ypad + fw/2 + fw), radius: fw/2, startAngle: CGFloat.pi * 3/2, endAngle: CGFloat.pi/2, clockwise: false)
//
        ctx.addCurve(to: CGPoint.init(x: xpad + fw + sp, y: fw * 2 + ypad),
                     control1: CGPoint.init(x: xpad, y: ypad),
                     control2: CGPoint.init(x: xpad + fw + sp + fw, y: ypad + fw * 3/2))
        
        ctx.strokePath()
        
        ctx.setLineWidth(w/3)
        ctx.addEllipse(in: CGRect.init(x: xpad + fw + fw, y: fw + ypad,
                                       width: fw/3, height: fw/3))
        ctx.addEllipse(in: CGRect.init(x: xpad + fw + fw + sp + fw/2, y: fw + ypad,
                                       width: fw/3, height: fw/3))
        
        ctx.move(to: CGPoint.init(x: xpad + fw + fw + sp + fw + sp, y: ypad + fw/2))
        ctx.addQuadCurve(to: CGPoint.init(x: xpad + fw + fw * 3 - sp, y: size.height - ypad),
                         control: CGPoint.init(x: xpad + fw + fw * 3 - sp - sp, y: size.height - ypad))
        yellow.setStroke()
        ctx.strokePath()
    }
}

extension NSString {
    @objc func width(font:UIFont)->CGFloat {
        let bounds = self.boundingRect(with: CGSize.init(width: kwidth, height: 40), options: NSStringDrawingOptions(rawValue: 0), attributes: [NSAttributedString.Key.font : font], context: nil)
        return bounds.size.width
    }
    @objc func size(font:UIFont, mSize:CGSize) -> CGSize {
        return size(mSize: mSize, ats: [NSAttributedString.Key.font : font])
    }
    
    @objc func size(mSize:CGSize, ats:[NSAttributedString.Key:Any]) -> CGSize {
        let bounds = self.boundingRect(with: mSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: ats, context: nil)
        return bounds.size
    }
    
}

